﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Lab12
{
    public partial class FormMain : Form
    {
        /// <summary>
        /// Данные, на основе которых строятся графики.
        /// </summary>
        DataTable data;

        /// <summary>
        /// Объект для рисования.
        /// </summary>
        Graphics graph;

        /// <summary>
        /// Закрытый конструктор, чтобы нельзя было создавать новые экземпляры этого класса.
        /// </summary>
        public FormMain()
        {
            InitializeComponent();

            DGV_Coords.RowHeadersWidth = 50;

            graph = P_Graph.CreateGraphics();
            graph.Clear(Color.White);

            // Изменить местоположение начала координат:
            graph.Clear(Color.White);
            graph.TranslateTransform(10, P_Graph.Height - 10);
            graph.RotateTransform(-90);
            // end
        }

        /// <summary>
        /// Происходит при нажатии кнопки "Открыть".
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void B_Open_Click(object sender, EventArgs e)
        {
            openFileDialog1.InitialDirectory = Application.StartupPath.Remove(Application.StartupPath.Length - 10) + @"\DataDefault";
            openFileDialog1.ShowDialog();
        }

        /// <summary>
        /// Происходит при нажатии кнопки "OK" в диалоге для окрытия файла OpenFileDialog.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void OpenFileDialog_OK(object sender, CancelEventArgs e)
        {
            try
            {
                string temp = FileHelper.ReadDataFromFile(openFileDialog1.FileName);
                data = DataTableHelper.CreateDataTable(temp);
                if (data.ColumnCount == 0) throw new Exception();
                else
                {
                    DGV_Coords.RowCount = 2;
                    DGV_Coords.ColumnCount = data.ColumnCount;

                    DGV_Coords.Rows[0].HeaderCell.Value = "X";
                    DGV_Coords.Rows[1].HeaderCell.Value = "Y";

                    for (int i = 0; i < 2; i++)
                        for (int j = 0; j < data.ColumnCount; j++)
                        {
                            DGV_Coords[j, i].Value = data[i, j];
                            DGV_Coords.Columns[j].Width = 25;
                        }

                    DGV_Coords.ClearSelection();

                    GB_Build.Enabled = true;
                }
            }
            catch (AccessViolationException)
            {
                MessageBox.Show("Данные не прочитаны", "Ошибка!", MessageBoxButtons.OK, MessageBoxIcon.Error, MessageBoxDefaultButton.Button1);
            }
        }

        private void B_Build_Click(object sender, EventArgs e)
        {
            graph.Clear(Color.White);

            // Максимальные значения координат x и y:
            float maxX = P_Graph.Width - 15;
            float maxY = P_Graph.Height - 15;

            // Рисование координатных линий:
            Pen penCoordsLine = new Pen(Color.LightSkyBlue, 3);
            penCoordsLine.EndCap = System.Drawing.Drawing2D.LineCap.ArrowAnchor;
            graph.DrawLine(penCoordsLine, 0, 0, 0, maxX); // ось x
            graph.DrawLine(penCoordsLine, 0, 0, maxY, 0); // ось y
            // end

            // Номер максимального значения:
            int numMaxValue = data.NumPointWithMaxY();

            if (RB_BarChart.Checked) // столбиковая диаграмма
            {
                // Шаг между столбиками:
                float deltaX = maxX / data.ColumnCount;

                // Масштаб для одного значения:
                float deltaY = maxY / data.MaxY();

                float currentX = deltaX / 2;
                for (int i = 0; i < data.ColumnCount; i++)
                {
                    PointF p1 = new PointF(0, currentX);
                    PointF p2 = new PointF(data[1, i] * deltaY, currentX);

                    Pen pen = new Pen(Color.ForestGreen, deltaX / 2);
                    if (i == numMaxValue) pen.Color = Color.Red;

                    graph.DrawLine(pen, p1, p2);

                    currentX += deltaX;
                }
            }
            else                    // ломаная
            {
                // Масштаб для одного значения:
                float deltaX = maxX / data.MaxX();

                // Масштаб для одного значения:
                float deltaY = maxY / data.MaxY();

                // Рисование линий между точками:
                for (int i = 1; i < data.ColumnCount; i++)
                {
                    PointF p1 = new PointF(data[1, i - 1] * deltaY, data[0, i - 1] * deltaX - deltaX / 2);
                    PointF p2 = new PointF(data[1, i] * deltaY, data[0, i] * deltaX - deltaX / 2);
                    Pen pen = new Pen(Color.LightGreen, 3);
                    graph.DrawLine(pen, p1, p2);
                }

                // Рисование самих точек:
                for (int i = 0; i < data.ColumnCount; i++)
                {
                    Pen pen = new Pen(Color.ForestGreen, 6);
                    if (i == numMaxValue) pen.Color = Color.Red;

                    graph.DrawEllipse(pen, (data[1, i] * deltaY - 3), (data[0, i] * deltaX - deltaX / 2 - 3), 6, 6);
                }
            }
        }
    }
}
