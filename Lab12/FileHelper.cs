﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using System.Windows.Forms;

namespace Lab12
{
    static class FileHelper
    {
        /// <summary>
        /// Считывает данные из файла, уоторый имеет путь filePath.
        /// </summary>
        /// <param name="fileName">Путь считываемого файла.</param>
        /// <returns></returns>
        public static string ReadDataFromFile(string filePath)
        {
            string res = null;
            StreamReader SR = null;
            try
            {
                SR = new StreamReader(filePath);
                res = SR.ReadToEnd();
                SR.Close();
            }
            catch
            {
                MessageBox.Show("Ошибка открытия файла!", "Ошибка!", MessageBoxButtons.OK, MessageBoxIcon.Error, MessageBoxDefaultButton.Button1);
            }
            return res;
        }
    }
}
