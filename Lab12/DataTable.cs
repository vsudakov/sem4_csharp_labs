﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Lab12
{
    class DataTable
    {
        /// <summary>
        /// Поле матрица целых чисел.
        /// </summary>
        float[,] data;

        /// <summary>
        /// Конструктор
        /// </summary>
        /// <param name="data">Массив с данными.</param>
        public DataTable(float[,] data)
        {
            this.data = data;
        }

        /// <summary>
        /// Индексатор для доступа к закрытому полю - двумерному массиву.
        /// </summary>
        /// <param name="i">Номер строки.</param>
        /// <param name="j">Номер столбца.</param>
        /// <returns></returns>
        public float this[int i, int j]
        {
            get { return data[i, j]; }
            set { data[i, j] = value; }
        }

        /// <summary>
        /// Число строк.
        /// </summary>
        public int RowCount
        {
            get
            {
                if (data != null)
                    return data.GetLength(0);
                return 0;
            }
        }

        /// <summary>
        /// Число столбцов.
        /// </summary>
        public int ColumnCount
        {
            get
            {
                if (data != null)
                    return data.Length / data.GetLength(0);
                return 0;
            }
        }

        /// <summary>
        /// Возвращает номер точки с максимальным числом Y.
        /// </summary>
        /// <returns></returns>
        public int NumPointWithMaxY()
        {
            float max = this[1, 0];
            int num = 0;
            for (int i = 0; i < this.ColumnCount; i++)
            {
                if (this[1, i] > max)
                {
                    max = this[1, i];
                    num = i;
                }
            }

            return num;
        }

        /// <summary>
        /// Возвращает максимальное число Y.
        /// </summary>
        /// <returns></returns>
        public float MaxY()
        {
            float max = this[1,0];
            for (int i = 0; i < this.ColumnCount; i++)
            {
                if (this[1, i] > max) max = this[1, i];
            }

            return max;
        }

        /// <summary>
        /// Возвращает максимальное число X.
        /// </summary>
        /// <returns></returns>
        public float MaxX()
        {
            float max = this[0, 0];
            for (int i = 0; i < this.ColumnCount; i++)
            {
                if (this[0, i] > max) max = this[0, i];
            }

            return max; 
        }
    }
}
