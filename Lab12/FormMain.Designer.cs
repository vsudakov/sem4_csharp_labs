﻿namespace Lab12
{
    partial class FormMain
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.B_Open = new System.Windows.Forms.Button();
            this.P_Graph = new System.Windows.Forms.Panel();
            this.GB_Build = new System.Windows.Forms.GroupBox();
            this.RB_BrokenLine = new System.Windows.Forms.RadioButton();
            this.RB_BarChart = new System.Windows.Forms.RadioButton();
            this.B_Build = new System.Windows.Forms.Button();
            this.DGV_Coords = new System.Windows.Forms.DataGridView();
            this.openFileDialog1 = new System.Windows.Forms.OpenFileDialog();
            this.GB_Build.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.DGV_Coords)).BeginInit();
            this.SuspendLayout();
            // 
            // B_Open
            // 
            this.B_Open.Location = new System.Drawing.Point(12, 12);
            this.B_Open.Name = "B_Open";
            this.B_Open.Size = new System.Drawing.Size(160, 24);
            this.B_Open.TabIndex = 0;
            this.B_Open.Text = "Открыть";
            this.B_Open.UseVisualStyleBackColor = true;
            this.B_Open.Click += new System.EventHandler(this.B_Open_Click);
            // 
            // P_Graph
            // 
            this.P_Graph.Location = new System.Drawing.Point(178, 12);
            this.P_Graph.Name = "P_Graph";
            this.P_Graph.Size = new System.Drawing.Size(436, 158);
            this.P_Graph.TabIndex = 1;
            // 
            // GB_Build
            // 
            this.GB_Build.Controls.Add(this.B_Build);
            this.GB_Build.Controls.Add(this.RB_BrokenLine);
            this.GB_Build.Controls.Add(this.RB_BarChart);
            this.GB_Build.Enabled = false;
            this.GB_Build.Location = new System.Drawing.Point(12, 42);
            this.GB_Build.Name = "GB_Build";
            this.GB_Build.Size = new System.Drawing.Size(160, 128);
            this.GB_Build.TabIndex = 0;
            this.GB_Build.TabStop = false;
            this.GB_Build.Text = "Построить";
            // 
            // RB_BrokenLine
            // 
            this.RB_BrokenLine.AutoSize = true;
            this.RB_BrokenLine.Location = new System.Drawing.Point(6, 42);
            this.RB_BrokenLine.Name = "RB_BrokenLine";
            this.RB_BrokenLine.Size = new System.Drawing.Size(71, 17);
            this.RB_BrokenLine.TabIndex = 0;
            this.RB_BrokenLine.Text = "Ломаная";
            this.RB_BrokenLine.UseVisualStyleBackColor = true;
            // 
            // RB_BarChart
            // 
            this.RB_BarChart.AutoSize = true;
            this.RB_BarChart.Checked = true;
            this.RB_BarChart.Location = new System.Drawing.Point(6, 19);
            this.RB_BarChart.Name = "RB_BarChart";
            this.RB_BarChart.Size = new System.Drawing.Size(151, 17);
            this.RB_BarChart.TabIndex = 1;
            this.RB_BarChart.TabStop = true;
            this.RB_BarChart.Text = "Столбиковая диаграмма";
            this.RB_BarChart.UseVisualStyleBackColor = true;
            // 
            // B_Build
            // 
            this.B_Build.BackColor = System.Drawing.Color.LightGreen;
            this.B_Build.Location = new System.Drawing.Point(6, 65);
            this.B_Build.Name = "B_Build";
            this.B_Build.Size = new System.Drawing.Size(148, 57);
            this.B_Build.TabIndex = 2;
            this.B_Build.Text = "Построить";
            this.B_Build.UseVisualStyleBackColor = false;
            this.B_Build.Click += new System.EventHandler(this.B_Build_Click);
            // 
            // DGV_Coords
            // 
            this.DGV_Coords.AllowUserToAddRows = false;
            this.DGV_Coords.AllowUserToDeleteRows = false;
            this.DGV_Coords.AllowUserToResizeColumns = false;
            this.DGV_Coords.AllowUserToResizeRows = false;
            this.DGV_Coords.BackgroundColor = System.Drawing.SystemColors.Control;
            this.DGV_Coords.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.DGV_Coords.ColumnHeadersVisible = false;
            this.DGV_Coords.Location = new System.Drawing.Point(13, 177);
            this.DGV_Coords.Name = "DGV_Coords";
            this.DGV_Coords.Size = new System.Drawing.Size(601, 63);
            this.DGV_Coords.TabIndex = 2;
            // 
            // openFileDialog1
            // 
            this.openFileDialog1.FileName = "openFileDialog1";
            this.openFileDialog1.FileOk += new System.ComponentModel.CancelEventHandler(this.OpenFileDialog_OK);
            // 
            // FormMain
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(626, 252);
            this.Controls.Add(this.DGV_Coords);
            this.Controls.Add(this.GB_Build);
            this.Controls.Add(this.P_Graph);
            this.Controls.Add(this.B_Open);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.MaximizeBox = false;
            this.Name = "FormMain";
            this.Text = "Лабораторная работа №12";
            this.GB_Build.ResumeLayout(false);
            this.GB_Build.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.DGV_Coords)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button B_Open;
        private System.Windows.Forms.Panel P_Graph;
        private System.Windows.Forms.GroupBox GB_Build;
        private System.Windows.Forms.Button B_Build;
        private System.Windows.Forms.RadioButton RB_BrokenLine;
        private System.Windows.Forms.RadioButton RB_BarChart;
        private System.Windows.Forms.DataGridView DGV_Coords;
        private System.Windows.Forms.OpenFileDialog openFileDialog1;
    }
}