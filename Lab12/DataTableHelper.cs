﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab12
{
    static class DataTableHelper
    {
        /// <summary>
        /// Создаёт объект класса DataTable из строки, содержащей данные из файла.
        /// </summary>
        /// <param name="dataFromFile">Строка, содержащая данные из файла.</param>
        /// <returns></returns>
        public static DataTable CreateDataTable(string dataFromFile)
        {
            string[] rows = dataFromFile.Split('\n');
            
            float[,] XY = new float[2, rows.Length];

            for (int i = 0; i < rows.Length; i++)
            {
                string[] temp = rows[i].Split(' ');
                XY[0, i] = SaveConvert.ToFloat_OrZero(temp[0]);
                XY[1, i] = SaveConvert.ToFloat_OrZero(temp[1]);
            }

            return new DataTable(XY);
        }
    }
}
