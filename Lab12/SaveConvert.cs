﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab12
{
    static class SaveConvert
    {
        /// <summary>
        /// Преобразует вещественное число, представленное в строке value, в переменную типа float.
        /// При ошибке конвертации ВОЗВРАЩАЕТ 0.
        /// </summary>
        /// <param name="value"></param>
        /// <returns></returns>
        public static float ToFloat_OrZero(string value)
        {
            try
            {
                return float.Parse(value);
            }
            catch
            {
                return 0;
            }
        }
    }
}
