﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Lab11
{
    class ExceptionHandler
    {
        /// <summary>
        /// Безопасное преобразование строки в int32. В случае ошибок возвращает значение "0".
        /// </summary>
        /// <param name="num">Преобразуемая строка.</param>
        /// <returns></returns>
        public static int ConvertToInt32_OrZero(string num)
        {
            int result = 0;
            try
            {
                result = Convert.ToInt32(num);
            }
            catch { }
            return result;
        }
    }
}
