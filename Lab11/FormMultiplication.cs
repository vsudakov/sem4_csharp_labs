﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace Lab11
{
    /// <summary>
    /// Вычисляет произведение заданных элементов матрицы.
    /// </summary>
    public partial class FormMultiplication : Form
    {
        /// <summary>
        /// Поле матрица.
        /// </summary>
        Matrix M;

        /// <summary>
        /// Формирует матрицу M + вызывает InitializeComponent().
        /// </summary>
        /// <param name="inputDGV"></param>
        public FormMultiplication(DataGridView inputDGV)
        {
            M = new Matrix(inputDGV);
            InitializeComponent();
        }

        /// <summary>
        /// Брабатывает событие "Открытие формы".
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void FormMultiplication_Load(object sender, EventArgs e)
        {
            BeginRow.Minimum = 1;
            BeginRow.Maximum = M.NumString;
            BeginColumn.Minimum = 1;
            BeginColumn.Maximum = M.NumColumn;
            EndRow.Minimum = 1;
            EndRow.Maximum = M.NumString;
            EndColumn.Minimum = 1;
            EndColumn.Maximum = M.NumColumn;
        }

        /// <summary>
        /// Переключатель стоит на элементе Mult1_RadioButton
        /// (Произведение элементов в заданном интервале)
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Mult1_CheckedChanged(object sender, EventArgs e)
        {
            Mult1_GroupBox.Enabled = true;
        }

        /// <summary>
        /// Переключатель стои на элеменнте Mult2_RadioButton
        /// (Произведение элементов, находящихся выше побочной диагонали)
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Mult2_CheckedChanged(object sender, EventArgs e)
        {
            Mult1_GroupBox.Enabled = false;
        }

        /// <summary>
        /// Клик по кнопке Calc_Button 
        /// (Вычислить)
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Calc_Click(object sender, EventArgs e)
        {
            if (Mult1_RadioButton.Checked)
            {
                Result_TextBox.Text = MatrixHelper.Multiplication(M, (int)BeginRow.Value - 1, (int)BeginColumn.Value - 1, (int)EndRow.Value - 1, (int)EndColumn.Value - 1).ToString();
            }
            else if (Mult2_RadioButton.Checked)
            {
                if (M.IsSquareMatrix)
                    Result_TextBox.Text = MatrixHelper.Multiplication(M).ToString();
                else
                    (new FormError("Матрица не является квадратной!")).ShowDialog();
            }
        }

        /// <summary>
        /// Изменение значения элемента BeginRow
        /// (Номер строки начала интервала)
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void BeginRow_ValueChanged(object sender, EventArgs e)
        {
            if (BeginRow.Value > EndRow.Value)
                EndRow.Value = BeginRow.Value;
            if (BeginRow.Value == EndRow.Value && BeginColumn.Value > EndColumn.Value)
                EndColumn.Value = BeginColumn.Value;
        }

        /// <summary>
        /// Изменение значения элемента BeginColumn
        /// (Номер столбца начала интервала)
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void BeginColumn_ValueChanged(object sender, EventArgs e)
        {
            if (BeginRow.Value == EndRow.Value && BeginColumn.Value > EndColumn.Value)
                EndColumn.Value = BeginColumn.Value;
        }

        /// <summary>
        /// Изменение значения элемента EndRow
        /// (Номер строки конца интервала)
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void EndRow_ValueChanged(object sender, EventArgs e)
        {
            if (EndRow.Value < BeginRow.Value)
                BeginRow.Value = EndRow.Value;
            if (BeginRow.Value == EndRow.Value && EndColumn.Value < BeginColumn.Value)
                BeginColumn.Value = EndColumn.Value;
        }

        /// <summary>
        /// Изменение значения элемента EndColumn
        /// (Номер столбца конца интервала)
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void EndColumn_ValueChanged(object sender, EventArgs e)
        {
            if (BeginRow.Value == EndRow.Value && EndColumn.Value < BeginColumn.Value)
                BeginColumn.Value = EndColumn.Value;
        }
    }
}
