﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Media;

namespace Lab11
{
    public partial class FormError : Form
    {
        /// <summary>
        /// Конструтор формы.
        /// </summary>
        /// <param name="errorMassage">Сообщение об ошибке.</param>
        public FormError(string errorMassage)
        {
            SystemSounds.Exclamation.Play();
            InitializeComponent();
            Massage_label.Text = errorMassage;
        }

        /// <summary>
        /// Кнопка "ОК"
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void OK_button_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}
