﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace Lab11
{
    class Matrix : ICloneable
    {
        /// <summary>
        /// Поле матрица целых чисел.
        /// </summary>
        int[,] M;

        /// <summary>
        /// Формирует пустую матрицу из numString строк и numColumn столбцов.
        /// </summary>
        /// <param name="numString">Число строок.</param>
        /// <param name="numColumn">Число столбцов.</param>
        public Matrix(int numString, int numColumn)
        {
            if (numString > 0 && numColumn > 0)
                M = new int[numString, numColumn];
        }

        /// <summary>
        /// Формирует матрицу из элементов inputDGV.
        /// </summary>
        /// <param name="inputDGV">Экземпляр класса DataGridView с ненулевым количеством строк и столбцов.</param>
        public Matrix(DataGridView inputDGV)
        {
            M = new int[inputDGV.RowCount, inputDGV.ColumnCount];
            for (int i = 0; i < this.NumString; i++)
                for (int j = 0; j < this.NumColumn; j++)
                    this[i, j] = (inputDGV[j, i].Value != null) ? ExceptionHandler.ConvertToInt32_OrZero(inputDGV[j, i].Value.ToString()) : 0;
        }

        /// <summary>
        /// Формирует матрицу из данных, считанных из файла.
        /// </summary>
        /// <param name="fileContent">Данные, считанные из файла.</param>
        public Matrix(string fileContent)
        {
            string[] rows = fileContent.Split('\n');
            List<string>[] allElements = new List<string>[rows.Length];
            for (int i = 0; i < rows.Length; i++)
                allElements[i] = new List<string>(rows[i].Split(' '));

            M = new int[allElements.Length, allElements[0].Count];
            for (int i = 0; i < this.NumString; i++)
                for (int j = 0; j < this.NumColumn; j++)
                    this[i, j] = ExceptionHandler.ConvertToInt32_OrZero(allElements[i][j]);
        }

        /// <summary>
        /// Индексатор для доступа к закрытому полю - двумерному массиву.
        /// </summary>
        /// <param name="i">Номер строки.</param>
        /// <param name="j">Номер столбца.</param>
        /// <returns></returns>
        public int this[int i, int j]
        {
            get { return M[i, j]; }
            set { M[i, j] = value; }
        }

        /// <summary>
        /// Число строк.
        /// </summary>
        public int NumString
        {
            get
            {
                if (M != null)
                    return M.GetLength(0);
                return 0;
            }
        }

        /// <summary>
        /// Число столбцов.
        /// </summary>
        public int NumColumn
        {
            get
            {
                if (M != null)
                    return M.Length / M.GetLength(0);
                return 0;
            }
        }

        /// <summary>
        /// Является ли матрица квадратной?
        /// </summary>
        public bool IsSquareMatrix
        {
            get
            {
                if (NumString == NumColumn)
                    return true;
                return false;
            }
        }

        /// <summary>
        /// Реализация интерфейса ICloneable
        /// </summary>
        /// <returns></returns>
        public object Clone()
        {
            Matrix res = (Matrix)this.MemberwiseClone();
            res.M = new int[this.NumString, this.NumColumn];
            for (int i = 0; i < this.NumString; i++)
                for (int j = 0; j < this.NumColumn; j++)
                    res[i, j] = this[i, j];
            return res;
        }
    }
}
