﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Media;
using System.IO;

namespace Lab11
{
    public partial class FormMain : Form
    {
        /// <summary>
        /// Конструктор по умолчанию.
        /// </summary>
        public FormMain()
        {
            InitializeComponent();
        }

        /// <summary>
        /// Обработка комбинаций клавиш для формы
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void FormMain_KeyUp(object sender, KeyEventArgs e)
        {
            if (e.Control && e.KeyCode == Keys.N) // Ctrl + N
                New_ToolStripMenuItem.PerformClick();
        }

        /// <summary>
        /// Создаёт новый файл.
        /// (Файл-Новый)
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void New_ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            ParametersMatrix_groupBox.Show();
            ProcessingMatrix_groupBox.Show();
            Seal_Button.Show();
            Save_Button.Show();
            InputMatrix_DGV.Show();
            ProcessingMatrix_ToolStripMenuItem.Enabled = true;
            Seal_TSMI.Enabled = true;
            Save_ToolStripMenuItem.Enabled = true;

            this.InputMatrix_DGV.RowCount = 0;
            this.InputMatrix_DGV.ColumnCount = 0;
            this.InputNumLines_textBox.Text = null;
            this.InputNumColumns_textBox.Text = null;
            this.InputNumLines_errorProvider.Clear();
            this.InputNumColumns_errorProvider.Clear();
            this.Sum_radioButton.Select();
            this.ActiveControl = this.InputNumLines_textBox;
        }

        /// <summary>
        /// Задаёт количество строк в элементе InputMatrix_DGV.
        /// (Осуществляется проверка валидности введённых данных)
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void InputNumLines_textBuox_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == (char)Keys.Enter)
                if (ExceptionHandler.ConvertToInt32_OrZero(InputNumLines_textBox.Text) != 0)
                {
                    InputMatrix_DGV.RowCount = ExceptionHandler.ConvertToInt32_OrZero(InputNumLines_textBox.Text);
                    for (int i = 0; i < InputMatrix_DGV.RowCount; i++)
                        InputMatrix_DGV.Rows[i].HeaderCell.Value = (i + 1).ToString();
                    InputNumLines_errorProvider.Clear();
                }
                else
                    InputNumLines_errorProvider.SetError(InputNumLines_textBox, "Невозможное\n  значение!");
        }

        /// <summary>
        /// Задаёт количество строк в элементе InputMatrix_DGV.
        /// (Осуществляется проверка валидности введённых данных)
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void InputNumLines_textBuox_Leave(object sender, EventArgs e)
        {
            this.InputNumLines_textBuox_KeyPress(null, new KeyPressEventArgs((char)Keys.Enter));
        }

        /// <summary>
        /// Задаёт количество столбцов в элементе InputMatrix_DGV.
        /// (Осуществляется проверка валидности введённых данных)
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void InputNumColumns_textBox_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == (char)Keys.Enter)
                if (ExceptionHandler.ConvertToInt32_OrZero(InputNumColumns_textBox.Text) != 0)
                {
                    InputMatrix_DGV.ColumnCount = ExceptionHandler.ConvertToInt32_OrZero(InputNumColumns_textBox.Text);
                    for (int i = 0; i < InputMatrix_DGV.ColumnCount; i++)
                    {
                        InputMatrix_DGV.Columns[i].HeaderText = (i + 1).ToString();
                        InputMatrix_DGV.Columns[i].Width = 25;
                    }
                    InputNumColumns_errorProvider.Clear();
                }
                else
                    InputNumColumns_errorProvider.SetError(InputNumColumns_textBox, "Невозможное\n  значение!");
        }

        /// <summary>
        /// Задаёт количество столбцов в элементе InputMatrix_DGV.
        /// (Осуществляется проверка валидности введённых данных)
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void InputNumColumns_textBox_Leave(object sender, EventArgs e)
        {
            this.InputNumColumns_textBox_KeyPress(null, new KeyPressEventArgs((char)Keys.Enter));
        }

        /// <summary>
        /// Определяет действие при нажатии на кнопку "Вычислить" в зависимости от установленного флажка.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Caculate_button_Click(object sender, EventArgs e)
        {
            if (Sum_radioButton.Checked) // если переключатель стоит на "Сумма"
            {
                this.Sum_TSMI.PerformClick();
            }
            else if (Multiplication_radioButton.Checked) // если переключатель стоит на "Произведение"
            {
                this.Multiplication_TSMI.PerformClick();
            }
            
        }

        /// <summary>
        /// Вызывает вычисление суммы всех элементов матрицы.
        /// (Обработка матрицы - Сумма)
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Sum_TSMI_Click(object sender, EventArgs e)
        {
            // если в элементе InputMatrix_DGV нет ни одной строки либо столбца . . .
            if (InputMatrix_DGV.RowCount == 0 || InputMatrix_DGV.ColumnCount == 0)
            {
                (new FormError("Матрица пуста!")).ShowDialog(this); // появляется окно с ошибкой
            }
            else
            {
                (new FormSum(InputMatrix_DGV)).ShowDialog(this);
            }
        }

        /// <summary>
        /// Вызывает вычисление произведения.
        /// (Обработка матрицы - Произведение . . .)
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Multiplication_TSMI_Click(object sender, EventArgs e)
        {
            // если в элементе InputMatrix_DGV нет ни одной строки либо столбца . . .
            if (InputMatrix_DGV.RowCount == 0 || InputMatrix_DGV.ColumnCount == 0)
            {
                (new FormError("Матрица пуста!")).ShowDialog(this); // появляется окно с ошибкой
            }
            else
            {
                (new FormMultiplication(InputMatrix_DGV)).ShowDialog(this);
            }
        }

        /// <summary>
        /// Уплотняет матрицу.
        /// (с пом. элемента Seal_TSMI)
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Seal_TSMI_Click(object sender, EventArgs e)
        {
            // если в элементе InputMatrix_DGV нет ни одной строки либо столбца . . .
            if (InputMatrix_DGV.RowCount == 0 || InputMatrix_DGV.ColumnCount == 0)
            {
                (new FormError("Матрица пуста!")).ShowDialog(this); // появляется окно с ошибкой
            }
            else
            {
                (new FormSeal(this,InputMatrix_DGV)).ShowDialog(this);
            }
        }

        /// <summary>
        /// Уплотняет матрицу.
        /// (с пом. элемента Seal_Button)
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Seal_Button_Click(object sender, EventArgs e)
        {
            this.Seal_TSMI.PerformClick();
        }

        /// <summary>
        /// Сохранить как . . .
        /// (Файл - Сохранить как . . .)
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Save_TSMI_Click(object sender, EventArgs e)
        {
            SaveFileDialog.ShowDialog();
        }

        /// <summary>
        /// Сохранить как . . .
        /// (кнопка Сохранить)
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Save_Button_Click(object sender, EventArgs e)
        {
            this.Save_ToolStripMenuItem.PerformClick();
        }

        /// <summary>
        /// Действие при нажатии кнопки "Сохранить" в диалоговом окне SaveFileDalog.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void SaveFileDialog_FileOk(object sender, CancelEventArgs e)
        {
            try
            {
                StreamWriter F = new StreamWriter(SaveFileDialog.FileName, false);

                Matrix M = new Matrix(this.InputMatrix_DGV); // cформировать матрицу из элементов data
                // записать матрицу в файл:
                for (int i = 0; i < M.NumString; i++)
                {
                    for (int j = 0; j < M.NumColumn; j++)
                    {
                        if (j != M.NumColumn - 1) F.Write(M[i, j].ToString() + " ");
                        else F.Write(M[i, j]);
                    }
                    if (i != M.NumString - 1) F.Write("\n");
                }

                F.Close();
            }
            catch { }
        }

        /// <summary>
        /// Открыть
        /// (Файл - Открыть)
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Open_TSMI_Click(object sender, EventArgs e)
        {
            OpenFileDialog.InitialDirectory = Application.StartupPath.Remove(Application.StartupPath.Length - 10);
            OpenFileDialog.ShowDialog();
        }

        /// <summary>
        /// Действие при нажатии кнопки "Открыть" в диалоговом окне OpenFileDalog
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void OpenFileDialog_FileOk(object sender, CancelEventArgs e)
        {
            this.New_ToolStripMenuItem.PerformClick();
            string fileName = OpenFileDialog.FileName;
            StreamReader F = new StreamReader(fileName);
            Matrix temp = new Matrix(F.ReadToEnd());
            MatrixHelper.AddToGridViewFromMatrix(temp, ref this.InputMatrix_DGV);
            this.InputNumLines_textBox.Text = this.InputMatrix_DGV.RowCount.ToString();
            this.InputNumColumns_textBox.Text = this.InputMatrix_DGV.ColumnCount.ToString();
        }

        /// <summary>
        /// Выход
        /// (Файл - Выход)
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Quit_TSMI_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        /// <summary>
        /// Об авторе.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void About_TSMI_Click(object sender, EventArgs e)
        {
            (new FormAbout()).Show();
        }
    }
}
