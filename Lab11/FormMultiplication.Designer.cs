﻿namespace Lab11
{
    partial class FormMultiplication
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.Mult1_RadioButton = new System.Windows.Forms.RadioButton();
            this.Mult2_RadioButton = new System.Windows.Forms.RadioButton();
            this.Mult1_GroupBox = new System.Windows.Forms.GroupBox();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.BeginRow = new System.Windows.Forms.NumericUpDown();
            this.BeginColumn = new System.Windows.Forms.NumericUpDown();
            this.EndRow = new System.Windows.Forms.NumericUpDown();
            this.EndColumn = new System.Windows.Forms.NumericUpDown();
            this.Calc_Button = new System.Windows.Forms.Button();
            this.Close_Button = new System.Windows.Forms.Button();
            this.label5 = new System.Windows.Forms.Label();
            this.Result_TextBox = new System.Windows.Forms.TextBox();
            this.Mult1_GroupBox.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.BeginRow)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.BeginColumn)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.EndRow)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.EndColumn)).BeginInit();
            this.SuspendLayout();
            // 
            // Mult1_RadioButton
            // 
            this.Mult1_RadioButton.Checked = true;
            this.Mult1_RadioButton.Location = new System.Drawing.Point(18, 5);
            this.Mult1_RadioButton.Name = "Mult1_RadioButton";
            this.Mult1_RadioButton.Size = new System.Drawing.Size(244, 32);
            this.Mult1_RadioButton.TabIndex = 0;
            this.Mult1_RadioButton.TabStop = true;
            this.Mult1_RadioButton.Text = "Произведение элементов в заданном интервале";
            this.Mult1_RadioButton.UseVisualStyleBackColor = true;
            this.Mult1_RadioButton.CheckedChanged += new System.EventHandler(this.Mult1_CheckedChanged);
            // 
            // Mult2_RadioButton
            // 
            this.Mult2_RadioButton.Location = new System.Drawing.Point(18, 137);
            this.Mult2_RadioButton.Name = "Mult2_RadioButton";
            this.Mult2_RadioButton.Size = new System.Drawing.Size(250, 36);
            this.Mult2_RadioButton.TabIndex = 5;
            this.Mult2_RadioButton.TabStop = true;
            this.Mult2_RadioButton.Text = "Произведение элементов, находящихся выше побочной диагонали";
            this.Mult2_RadioButton.UseVisualStyleBackColor = true;
            this.Mult2_RadioButton.CheckedChanged += new System.EventHandler(this.Mult2_CheckedChanged);
            // 
            // Mult1_GroupBox
            // 
            this.Mult1_GroupBox.Controls.Add(this.label4);
            this.Mult1_GroupBox.Controls.Add(this.label3);
            this.Mult1_GroupBox.Controls.Add(this.label2);
            this.Mult1_GroupBox.Controls.Add(this.label1);
            this.Mult1_GroupBox.Controls.Add(this.BeginRow);
            this.Mult1_GroupBox.Controls.Add(this.BeginColumn);
            this.Mult1_GroupBox.Controls.Add(this.EndRow);
            this.Mult1_GroupBox.Controls.Add(this.EndColumn);
            this.Mult1_GroupBox.Location = new System.Drawing.Point(12, 36);
            this.Mult1_GroupBox.Name = "Mult1_GroupBox";
            this.Mult1_GroupBox.Size = new System.Drawing.Size(256, 88);
            this.Mult1_GroupBox.TabIndex = 2;
            this.Mult1_GroupBox.TabStop = false;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(4, 69);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(97, 13);
            this.label4.TabIndex = 8;
            this.label4.Text = "Конец интервала:";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(3, 33);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(103, 13);
            this.label3.TabIndex = 7;
            this.label3.Text = "Начало интервала:";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(188, 10);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(62, 13);
            this.label2.TabIndex = 6;
            this.label2.Text = "№ столбца";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(115, 10);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(56, 13);
            this.label1.TabIndex = 5;
            this.label1.Text = "№ строки";
            // 
            // BeginRow
            // 
            this.BeginRow.Location = new System.Drawing.Point(118, 32);
            this.BeginRow.Name = "BeginRow";
            this.BeginRow.Size = new System.Drawing.Size(50, 20);
            this.BeginRow.TabIndex = 1;
            this.BeginRow.ValueChanged += new System.EventHandler(this.BeginRow_ValueChanged);
            // 
            // BeginColumn
            // 
            this.BeginColumn.Location = new System.Drawing.Point(191, 32);
            this.BeginColumn.Name = "BeginColumn";
            this.BeginColumn.Size = new System.Drawing.Size(50, 20);
            this.BeginColumn.TabIndex = 2;
            this.BeginColumn.ValueChanged += new System.EventHandler(this.BeginColumn_ValueChanged);
            // 
            // EndRow
            // 
            this.EndRow.Location = new System.Drawing.Point(118, 66);
            this.EndRow.Name = "EndRow";
            this.EndRow.Size = new System.Drawing.Size(50, 20);
            this.EndRow.TabIndex = 3;
            this.EndRow.ValueChanged += new System.EventHandler(this.EndRow_ValueChanged);
            // 
            // EndColumn
            // 
            this.EndColumn.Location = new System.Drawing.Point(191, 66);
            this.EndColumn.Name = "EndColumn";
            this.EndColumn.Size = new System.Drawing.Size(50, 20);
            this.EndColumn.TabIndex = 4;
            this.EndColumn.ValueChanged += new System.EventHandler(this.EndColumn_ValueChanged);
            // 
            // Calc_Button
            // 
            this.Calc_Button.Location = new System.Drawing.Point(33, 223);
            this.Calc_Button.Name = "Calc_Button";
            this.Calc_Button.Size = new System.Drawing.Size(97, 23);
            this.Calc_Button.TabIndex = 6;
            this.Calc_Button.Text = "Вычислить";
            this.Calc_Button.UseVisualStyleBackColor = true;
            this.Calc_Button.Click += new System.EventHandler(this.Calc_Click);
            // 
            // Close_Button
            // 
            this.Close_Button.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.Close_Button.Location = new System.Drawing.Point(151, 223);
            this.Close_Button.Name = "Close_Button";
            this.Close_Button.Size = new System.Drawing.Size(97, 23);
            this.Close_Button.TabIndex = 7;
            this.Close_Button.Text = "Закрыть";
            this.Close_Button.UseVisualStyleBackColor = true;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(16, 190);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(62, 13);
            this.label5.TabIndex = 8;
            this.label5.Text = "Результат:";
            // 
            // Result_TextBox
            // 
            this.Result_TextBox.Location = new System.Drawing.Point(83, 187);
            this.Result_TextBox.Name = "Result_TextBox";
            this.Result_TextBox.ReadOnly = true;
            this.Result_TextBox.Size = new System.Drawing.Size(100, 20);
            this.Result_TextBox.TabIndex = 9;
            // 
            // FormMultiplication
            // 
            this.AcceptButton = this.Calc_Button;
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.CancelButton = this.Close_Button;
            this.ClientSize = new System.Drawing.Size(280, 261);
            this.Controls.Add(this.Result_TextBox);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.Close_Button);
            this.Controls.Add(this.Calc_Button);
            this.Controls.Add(this.Mult1_GroupBox);
            this.Controls.Add(this.Mult2_RadioButton);
            this.Controls.Add(this.Mult1_RadioButton);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "FormMultiplication";
            this.ShowIcon = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Произведение . . .";
            this.Load += new System.EventHandler(this.FormMultiplication_Load);
            this.Mult1_GroupBox.ResumeLayout(false);
            this.Mult1_GroupBox.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.BeginRow)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.BeginColumn)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.EndRow)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.EndColumn)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.RadioButton Mult1_RadioButton;
        private System.Windows.Forms.RadioButton Mult2_RadioButton;
        private System.Windows.Forms.GroupBox Mult1_GroupBox;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.NumericUpDown BeginRow;
        private System.Windows.Forms.NumericUpDown BeginColumn;
        private System.Windows.Forms.NumericUpDown EndRow;
        private System.Windows.Forms.NumericUpDown EndColumn;
        private System.Windows.Forms.Button Calc_Button;
        private System.Windows.Forms.Button Close_Button;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox Result_TextBox;
    }
}