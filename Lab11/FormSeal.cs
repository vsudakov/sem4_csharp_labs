﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace Lab11
{
    public partial class FormSeal : Form
    {
        /// <summary>
        /// Поле-ссылка на вызывающую форму.
        /// </summary>
        FormMain parent;
        
        /// <summary>
        /// Поле матрица.
        /// </summary>
        Matrix M;

        /// <summary>
        /// Конструктор формы. Создаёт форму, сохраняя ссылку на вызывающую форму, а так же
        /// получает данные из вызывающей формы.
        /// </summary>
        /// <param name="parent"></param>
        /// <param name="source"></param>
        public FormSeal(FormMain parent, DataGridView source)
        {
            this.parent = parent;
            M = new Matrix(source);
            InitializeComponent();
        }

        /// <summary>
        /// Обрабатывает событие "Открытие формы"
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void FormSeal_Load(object sender, EventArgs e)
        {
            MatrixHelper.AddToGridViewFromMatrix(M, ref BeginMatrix_DGV);
            MatrixHelper.AddToGridViewFromMatrix(MatrixHelper.Seal(M), ref EndMatrix_DGV);
        }

        /// <summary>
        /// Кнопка "Применить"
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Accept_Button_Click(object sender, EventArgs e)
        {
            MatrixHelper.AddToGridViewFromMatrix(MatrixHelper.Seal(M),ref parent.InputMatrix_DGV);
            parent.InputNumLines_textBox.Text = parent.InputMatrix_DGV.RowCount.ToString();
            parent.InputNumColumns_textBox.Text = parent.InputMatrix_DGV.ColumnCount.ToString();
            MessageBox.Show("Изменения применены.", "Сообщение", MessageBoxButtons.OK, MessageBoxIcon.Information, MessageBoxDefaultButton.Button1);
            this.Close();
        }
    }
}
