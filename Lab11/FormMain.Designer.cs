﻿namespace Lab11
{
    partial class FormMain
    {
        /// <summary>
        /// Требуется переменная конструктора.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Освободить все используемые ресурсы.
        /// </summary>
        /// <param name="disposing">истинно, если управляемый ресурс должен быть удален; иначе ложно.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Код, автоматически созданный конструктором форм Windows

        /// <summary>
        /// Обязательный метод для поддержки конструктора - не изменяйте
        /// содержимое данного метода при помощи редактора кода.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle10 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle11 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle12 = new System.Windows.Forms.DataGridViewCellStyle();
            this.Main_MenuStrip = new System.Windows.Forms.MenuStrip();
            this.fileToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.New_ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.Open_ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.Save_ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.Quit_ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.ProcessingMatrix_ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.Sum_TSMI = new System.Windows.Forms.ToolStripMenuItem();
            this.Multiplication_TSMI = new System.Windows.Forms.ToolStripMenuItem();
            this.Seal_TSMI = new System.Windows.Forms.ToolStripMenuItem();
            this.About_ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.InputNumLines_textBox = new System.Windows.Forms.TextBox();
            this.InputNumColumns_textBox = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.InputMatrix_DGV = new System.Windows.Forms.DataGridView();
            this.ParametersMatrix_groupBox = new System.Windows.Forms.GroupBox();
            this.ProcessingMatrix_groupBox = new System.Windows.Forms.GroupBox();
            this.Calculate_button = new System.Windows.Forms.Button();
            this.Multiplication_radioButton = new System.Windows.Forms.RadioButton();
            this.Sum_radioButton = new System.Windows.Forms.RadioButton();
            this.InputNumLines_errorProvider = new System.Windows.Forms.ErrorProvider(this.components);
            this.InputNumColumns_errorProvider = new System.Windows.Forms.ErrorProvider(this.components);
            this.Save_Button = new System.Windows.Forms.Button();
            this.Seal_Button = new System.Windows.Forms.Button();
            this.SaveFileDialog = new System.Windows.Forms.SaveFileDialog();
            this.OpenFileDialog = new System.Windows.Forms.OpenFileDialog();
            this.Main_MenuStrip.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.InputMatrix_DGV)).BeginInit();
            this.ParametersMatrix_groupBox.SuspendLayout();
            this.ProcessingMatrix_groupBox.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.InputNumLines_errorProvider)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.InputNumColumns_errorProvider)).BeginInit();
            this.SuspendLayout();
            // 
            // Main_MenuStrip
            // 
            this.Main_MenuStrip.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.fileToolStripMenuItem,
            this.ProcessingMatrix_ToolStripMenuItem,
            this.Seal_TSMI,
            this.About_ToolStripMenuItem});
            this.Main_MenuStrip.Location = new System.Drawing.Point(0, 0);
            this.Main_MenuStrip.Name = "Main_MenuStrip";
            this.Main_MenuStrip.Size = new System.Drawing.Size(733, 24);
            this.Main_MenuStrip.TabIndex = 0;
            this.Main_MenuStrip.Text = "menuStrip1";
            // 
            // fileToolStripMenuItem
            // 
            this.fileToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.New_ToolStripMenuItem,
            this.Open_ToolStripMenuItem,
            this.Save_ToolStripMenuItem,
            this.Quit_ToolStripMenuItem});
            this.fileToolStripMenuItem.Name = "fileToolStripMenuItem";
            this.fileToolStripMenuItem.Size = new System.Drawing.Size(48, 20);
            this.fileToolStripMenuItem.Text = "Файл";
            // 
            // New_ToolStripMenuItem
            // 
            this.New_ToolStripMenuItem.Name = "New_ToolStripMenuItem";
            this.New_ToolStripMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.N)));
            this.New_ToolStripMenuItem.Size = new System.Drawing.Size(243, 22);
            this.New_ToolStripMenuItem.Text = "Новый";
            this.New_ToolStripMenuItem.Click += new System.EventHandler(this.New_ToolStripMenuItem_Click);
            // 
            // Open_ToolStripMenuItem
            // 
            this.Open_ToolStripMenuItem.Name = "Open_ToolStripMenuItem";
            this.Open_ToolStripMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.O)));
            this.Open_ToolStripMenuItem.Size = new System.Drawing.Size(243, 22);
            this.Open_ToolStripMenuItem.Text = "Открыть";
            this.Open_ToolStripMenuItem.Click += new System.EventHandler(this.Open_TSMI_Click);
            // 
            // Save_ToolStripMenuItem
            // 
            this.Save_ToolStripMenuItem.Enabled = false;
            this.Save_ToolStripMenuItem.Name = "Save_ToolStripMenuItem";
            this.Save_ToolStripMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys)(((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.Shift) 
            | System.Windows.Forms.Keys.S)));
            this.Save_ToolStripMenuItem.Size = new System.Drawing.Size(243, 22);
            this.Save_ToolStripMenuItem.Text = "Сохранить как . . .";
            this.Save_ToolStripMenuItem.Click += new System.EventHandler(this.Save_TSMI_Click);
            // 
            // Quit_ToolStripMenuItem
            // 
            this.Quit_ToolStripMenuItem.Name = "Quit_ToolStripMenuItem";
            this.Quit_ToolStripMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Alt | System.Windows.Forms.Keys.F4)));
            this.Quit_ToolStripMenuItem.Size = new System.Drawing.Size(243, 22);
            this.Quit_ToolStripMenuItem.Text = "Выход";
            this.Quit_ToolStripMenuItem.Click += new System.EventHandler(this.Quit_TSMI_Click);
            // 
            // ProcessingMatrix_ToolStripMenuItem
            // 
            this.ProcessingMatrix_ToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.Sum_TSMI,
            this.Multiplication_TSMI});
            this.ProcessingMatrix_ToolStripMenuItem.Enabled = false;
            this.ProcessingMatrix_ToolStripMenuItem.Name = "ProcessingMatrix_ToolStripMenuItem";
            this.ProcessingMatrix_ToolStripMenuItem.Size = new System.Drawing.Size(132, 20);
            this.ProcessingMatrix_ToolStripMenuItem.Text = "Обработка матрицы";
            // 
            // Sum_TSMI
            // 
            this.Sum_TSMI.Name = "Sum_TSMI";
            this.Sum_TSMI.Size = new System.Drawing.Size(171, 22);
            this.Sum_TSMI.Text = "Сумма";
            this.Sum_TSMI.Click += new System.EventHandler(this.Sum_TSMI_Click);
            // 
            // Multiplication_TSMI
            // 
            this.Multiplication_TSMI.Name = "Multiplication_TSMI";
            this.Multiplication_TSMI.Size = new System.Drawing.Size(171, 22);
            this.Multiplication_TSMI.Text = "Произведение . . .";
            this.Multiplication_TSMI.Click += new System.EventHandler(this.Multiplication_TSMI_Click);
            // 
            // Seal_TSMI
            // 
            this.Seal_TSMI.Enabled = false;
            this.Seal_TSMI.Name = "Seal_TSMI";
            this.Seal_TSMI.Size = new System.Drawing.Size(77, 20);
            this.Seal_TSMI.Text = "Уплотнить";
            this.Seal_TSMI.Click += new System.EventHandler(this.Seal_TSMI_Click);
            // 
            // About_ToolStripMenuItem
            // 
            this.About_ToolStripMenuItem.Name = "About_ToolStripMenuItem";
            this.About_ToolStripMenuItem.ShortcutKeys = System.Windows.Forms.Keys.F1;
            this.About_ToolStripMenuItem.Size = new System.Drawing.Size(75, 20);
            this.About_ToolStripMenuItem.Text = "Об авторе";
            this.About_ToolStripMenuItem.Click += new System.EventHandler(this.About_TSMI_Click);
            // 
            // InputNumLines_textBox
            // 
            this.InputNumLines_textBox.Location = new System.Drawing.Point(113, 20);
            this.InputNumLines_textBox.Name = "InputNumLines_textBox";
            this.InputNumLines_textBox.Size = new System.Drawing.Size(59, 20);
            this.InputNumLines_textBox.TabIndex = 1;
            this.InputNumLines_textBox.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.InputNumLines_textBuox_KeyPress);
            this.InputNumLines_textBox.Leave += new System.EventHandler(this.InputNumLines_textBuox_Leave);
            // 
            // InputNumColumns_textBox
            // 
            this.InputNumColumns_textBox.Location = new System.Drawing.Point(113, 45);
            this.InputNumColumns_textBox.Name = "InputNumColumns_textBox";
            this.InputNumColumns_textBox.Size = new System.Drawing.Size(59, 20);
            this.InputNumColumns_textBox.TabIndex = 2;
            this.InputNumColumns_textBox.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.InputNumColumns_textBox_KeyPress);
            this.InputNumColumns_textBox.Leave += new System.EventHandler(this.InputNumColumns_textBox_Leave);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(14, 23);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(74, 13);
            this.label1.TabIndex = 3;
            this.label1.Text = "Число строк:";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(15, 48);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(92, 13);
            this.label2.TabIndex = 4;
            this.label2.Text = "Число столбцов:";
            // 
            // InputMatrix_DGV
            // 
            this.InputMatrix_DGV.AllowUserToAddRows = false;
            dataGridViewCellStyle10.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle10.BackColor = System.Drawing.SystemColors.ActiveCaption;
            dataGridViewCellStyle10.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            dataGridViewCellStyle10.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle10.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle10.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle10.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.InputMatrix_DGV.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle10;
            this.InputMatrix_DGV.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            dataGridViewCellStyle11.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle11.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle11.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            dataGridViewCellStyle11.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle11.NullValue = "0";
            dataGridViewCellStyle11.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle11.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle11.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.InputMatrix_DGV.DefaultCellStyle = dataGridViewCellStyle11;
            this.InputMatrix_DGV.Location = new System.Drawing.Point(213, 36);
            this.InputMatrix_DGV.Name = "InputMatrix_DGV";
            dataGridViewCellStyle12.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle12.BackColor = System.Drawing.SystemColors.ActiveCaption;
            dataGridViewCellStyle12.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            dataGridViewCellStyle12.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle12.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle12.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle12.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.InputMatrix_DGV.RowHeadersDefaultCellStyle = dataGridViewCellStyle12;
            this.InputMatrix_DGV.RowHeadersWidth = 45;
            this.InputMatrix_DGV.Size = new System.Drawing.Size(508, 199);
            this.InputMatrix_DGV.TabIndex = 5;
            this.InputMatrix_DGV.Visible = false;
            // 
            // ParametersMatrix_groupBox
            // 
            this.ParametersMatrix_groupBox.Controls.Add(this.label1);
            this.ParametersMatrix_groupBox.Controls.Add(this.InputNumLines_textBox);
            this.ParametersMatrix_groupBox.Controls.Add(this.label2);
            this.ParametersMatrix_groupBox.Controls.Add(this.InputNumColumns_textBox);
            this.ParametersMatrix_groupBox.Location = new System.Drawing.Point(9, 27);
            this.ParametersMatrix_groupBox.Name = "ParametersMatrix_groupBox";
            this.ParametersMatrix_groupBox.Size = new System.Drawing.Size(188, 72);
            this.ParametersMatrix_groupBox.TabIndex = 6;
            this.ParametersMatrix_groupBox.TabStop = false;
            this.ParametersMatrix_groupBox.Text = "Параметры матрицы";
            this.ParametersMatrix_groupBox.Visible = false;
            // 
            // ProcessingMatrix_groupBox
            // 
            this.ProcessingMatrix_groupBox.Controls.Add(this.Calculate_button);
            this.ProcessingMatrix_groupBox.Controls.Add(this.Multiplication_radioButton);
            this.ProcessingMatrix_groupBox.Controls.Add(this.Sum_radioButton);
            this.ProcessingMatrix_groupBox.Location = new System.Drawing.Point(9, 106);
            this.ProcessingMatrix_groupBox.Name = "ProcessingMatrix_groupBox";
            this.ProcessingMatrix_groupBox.Size = new System.Drawing.Size(188, 96);
            this.ProcessingMatrix_groupBox.TabIndex = 7;
            this.ProcessingMatrix_groupBox.TabStop = false;
            this.ProcessingMatrix_groupBox.Text = "Обработка матрицы";
            this.ProcessingMatrix_groupBox.Visible = false;
            // 
            // Calculate_button
            // 
            this.Calculate_button.Location = new System.Drawing.Point(56, 67);
            this.Calculate_button.Name = "Calculate_button";
            this.Calculate_button.Size = new System.Drawing.Size(75, 23);
            this.Calculate_button.TabIndex = 2;
            this.Calculate_button.Text = "Вычислить";
            this.Calculate_button.UseVisualStyleBackColor = true;
            this.Calculate_button.Click += new System.EventHandler(this.Caculate_button_Click);
            // 
            // Multiplication_radioButton
            // 
            this.Multiplication_radioButton.AutoSize = true;
            this.Multiplication_radioButton.Location = new System.Drawing.Point(6, 43);
            this.Multiplication_radioButton.Name = "Multiplication_radioButton";
            this.Multiplication_radioButton.Size = new System.Drawing.Size(117, 17);
            this.Multiplication_radioButton.TabIndex = 1;
            this.Multiplication_radioButton.Text = "Произведение . . .";
            this.Multiplication_radioButton.UseVisualStyleBackColor = true;
            // 
            // Sum_radioButton
            // 
            this.Sum_radioButton.AutoSize = true;
            this.Sum_radioButton.Checked = true;
            this.Sum_radioButton.Location = new System.Drawing.Point(7, 20);
            this.Sum_radioButton.Name = "Sum_radioButton";
            this.Sum_radioButton.Size = new System.Drawing.Size(59, 17);
            this.Sum_radioButton.TabIndex = 0;
            this.Sum_radioButton.TabStop = true;
            this.Sum_radioButton.Text = "Сумма";
            this.Sum_radioButton.UseVisualStyleBackColor = true;
            // 
            // InputNumLines_errorProvider
            // 
            this.InputNumLines_errorProvider.ContainerControl = this;
            // 
            // InputNumColumns_errorProvider
            // 
            this.InputNumColumns_errorProvider.ContainerControl = this;
            // 
            // Save_Button
            // 
            this.Save_Button.Location = new System.Drawing.Point(92, 212);
            this.Save_Button.Name = "Save_Button";
            this.Save_Button.Size = new System.Drawing.Size(108, 23);
            this.Save_Button.TabIndex = 9;
            this.Save_Button.Text = "Сохранить как . . .";
            this.Save_Button.UseVisualStyleBackColor = true;
            this.Save_Button.Visible = false;
            this.Save_Button.Click += new System.EventHandler(this.Save_Button_Click);
            // 
            // Seal_Button
            // 
            this.Seal_Button.Location = new System.Drawing.Point(9, 212);
            this.Seal_Button.Name = "Seal_Button";
            this.Seal_Button.Size = new System.Drawing.Size(75, 23);
            this.Seal_Button.TabIndex = 8;
            this.Seal_Button.Text = "Уплотнить";
            this.Seal_Button.UseVisualStyleBackColor = true;
            this.Seal_Button.Visible = false;
            this.Seal_Button.Click += new System.EventHandler(this.Seal_Button_Click);
            // 
            // SaveFileDialog
            // 
            this.SaveFileDialog.DefaultExt = "txt";
            this.SaveFileDialog.Filter = "Тестовые файлы|*.txt|Все файлы|*.*";
            this.SaveFileDialog.Title = "Сохранить как . . .";
            this.SaveFileDialog.FileOk += new System.ComponentModel.CancelEventHandler(this.SaveFileDialog_FileOk);
            // 
            // OpenFileDialog
            // 
            this.OpenFileDialog.DefaultExt = "txt";
            this.OpenFileDialog.Filter = "Тестовые файлы|*.txt|Все файлы|*.*";
            this.OpenFileDialog.Title = "Открыть";
            this.OpenFileDialog.FileOk += new System.ComponentModel.CancelEventHandler(this.OpenFileDialog_FileOk);
            // 
            // FormMain
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(733, 247);
            this.Controls.Add(this.Seal_Button);
            this.Controls.Add(this.Save_Button);
            this.Controls.Add(this.ProcessingMatrix_groupBox);
            this.Controls.Add(this.ParametersMatrix_groupBox);
            this.Controls.Add(this.InputMatrix_DGV);
            this.Controls.Add(this.Main_MenuStrip);
            this.MainMenuStrip = this.Main_MenuStrip;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "FormMain";
            this.Text = "Лабораторная работа №11";
            this.KeyUp += new System.Windows.Forms.KeyEventHandler(this.FormMain_KeyUp);
            this.Main_MenuStrip.ResumeLayout(false);
            this.Main_MenuStrip.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.InputMatrix_DGV)).EndInit();
            this.ParametersMatrix_groupBox.ResumeLayout(false);
            this.ParametersMatrix_groupBox.PerformLayout();
            this.ProcessingMatrix_groupBox.ResumeLayout(false);
            this.ProcessingMatrix_groupBox.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.InputNumLines_errorProvider)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.InputNumColumns_errorProvider)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.MenuStrip Main_MenuStrip;
        private System.Windows.Forms.ToolStripMenuItem fileToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem New_ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem Open_ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem Save_ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem Quit_ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem ProcessingMatrix_ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem Sum_TSMI;
        private System.Windows.Forms.ToolStripMenuItem Multiplication_TSMI;
        private System.Windows.Forms.ToolStripMenuItem Seal_TSMI;
        private System.Windows.Forms.ToolStripMenuItem About_ToolStripMenuItem;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.GroupBox ParametersMatrix_groupBox;
        private System.Windows.Forms.GroupBox ProcessingMatrix_groupBox;
        private System.Windows.Forms.Button Calculate_button;
        private System.Windows.Forms.RadioButton Multiplication_radioButton;
        private System.Windows.Forms.RadioButton Sum_radioButton;
        private System.Windows.Forms.ErrorProvider InputNumLines_errorProvider;
        private System.Windows.Forms.ErrorProvider InputNumColumns_errorProvider;
        private System.Windows.Forms.Button Save_Button;
        private System.Windows.Forms.Button Seal_Button;
        public System.Windows.Forms.DataGridView InputMatrix_DGV;
        public System.Windows.Forms.TextBox InputNumLines_textBox;
        public System.Windows.Forms.TextBox InputNumColumns_textBox;
        private System.Windows.Forms.SaveFileDialog SaveFileDialog;
        private System.Windows.Forms.OpenFileDialog OpenFileDialog;
    }
}

