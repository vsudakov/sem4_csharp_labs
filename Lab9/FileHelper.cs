﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;

namespace Lab_9_Task_1
{
    static class FileHelper
    {
        /// <summary>
        /// Открывает текстовый файл.
        /// </summary>
        /// <returns></returns>
        public static StreamReader OpenFile()
        {
            StreamReader F = null;
            bool exit = false;
            while (!exit)
            {
                try
                {
                    Console.Write("Введите имя файла: ");
                    string nameFile = Console.ReadLine();
                    F = new StreamReader(@nameFile, Encoding.Default);
                    Console.ForegroundColor = ConsoleColor.Green;
                    Console.WriteLine("Файл с имнем \"" + nameFile + "\" успешно открыт.");
                    Console.ResetColor();
                    exit = true;
                }
                catch (FileNotFoundException)
                {
                    Console.ForegroundColor = ConsoleColor.Red;
                    Console.WriteLine("\nошибка: Файл с указанным именем не существует!\n");
                    Console.ResetColor();
                }
                catch (Exception e)
                {
                    Console.ForegroundColor = ConsoleColor.Red;
                    Console.WriteLine("\nошибка: " + e.Message + "\n");
                    Console.ResetColor();
                }
            }
            return F;
        }

        /// <summary>
        /// Создаёт список из строк с информацией для массива структур.
        /// </summary>
        /// <param name="F">Поток(файл), содержащий строки с информацией.</param>
        /// <returns></returns>
        public static List<string> ReadFromFile(StreamReader F)
        {
            List<string> res = new List<string>(); // список из считанных строк
            string buf;

            while ((buf = F.ReadLine()) != null)
                res.Add(buf);

            F.Close();

            return res;
        }

        /// <summary>
        /// Создаёт новый поток (файл) для записи.
        /// </summary>
        /// <param name="nameFile">Имя создаваемого файла.</param>
        /// <returns></returns>
        public static StreamWriter CreateStreamWriter(string nameFile)
        {
            StreamWriter F = null;
            bool exit = false;
            while (!exit)
            {
                try
                {
                    F = new StreamWriter(nameFile, false, Encoding.Unicode);
                    Console.Write("Файл с имнем \"" + nameFile + "\" успешно создан. ");
                    exit = true;
                }
                catch (Exception e)
                {
                    Console.ForegroundColor = ConsoleColor.Red;
                    Console.WriteLine("\nошибка: " + e.Message + "\n");
                    Console.ResetColor();
                }
            }
            return F;
        }

        /// <summary>
        /// Выводит информацию о подписчиках на журнал journ в поток F и закрывает этот поток.
        /// </summary>
        /// <param name="journ">Выводимый журнал.</param>
        /// <param name="subs">Массив подписчиков.</param>
        /// <param name="F">Поток для вывода.</param>
        public static void PrintInAFileOneJourn(Sub.Pressa journ, Sub[] subs, StreamWriter F)
        {
            if (!SubHelper.IsThereSubToThisJourn(journ, subs))
            {
                F.Write("Подписчики на журнал \"{0}\" не обнаружены!", journ);
            }
            else
            {
                F.WriteLine("Подписчики на журнал \"{0}\":", journ);
                F.WriteLine("┌───┬───────────────────┬─────────────────────────┬───────────────────────────┐");
                F.WriteLine("│ № │      Фамилия      │          Адрес          │      Другие издания       │");
                F.WriteLine("├───┼───────────────────┼─────────────────────────┼───────────────────────────┤");
                int numInTable = 0;
                for (int i = 0; i < subs.Length; i++)
                    if (subs[i].IsThereThisJourn(journ))
                        subs[i].DisplayInATableInAFile(++numInTable, journ, F);
                F.Write("└───┴───────────────────┴─────────────────────────┴───────────────────────────┘");
            }
            F.Close();
            Console.WriteLine("Информация выведена.");
        }

        /// <summary>
        /// Выводит в разные файлы информацию о подписчиках на каждый журнал. Имя файла соответствует названию журнала. В каждом файле фамилии отсортированы в алфавитном порядке.
        /// </summary>
        /// <param name="subs">Массив подписчиков.</param>
        public static void PrintInAFileAllJourns(Sub[] subs)
        {
            Array.Sort(subs); // сортирует массив подписчиков по фамилии.
            for (int i = 0; i < 5; i++)
            {
                Sub.Pressa tempP = (Sub.Pressa)i;
                string fileName = tempP.ToString() + ".txt";
                PrintInAFileOneJourn(tempP, subs, CreateStreamWriter(fileName));
            }
        }
    }
}
