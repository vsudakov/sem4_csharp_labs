﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Lab_9_Task_1
{
    static class SubHelper
    {
        /// <summary>
        /// Формирует массив подписчиков из строк, содержащих данные о них.
        /// </summary>
        /// <param name="L">Список строк, содержащих данные.</param>
        /// <returns></returns>
        public static Sub[] FormerArrayOfSubs(List<string> L)
        {
            Sub[] res = new Sub[L.Count]; // создание результирующего массива структур

            for (int i = 0; i < L.Count; i++)
            {
                string[] data = L[i].Split(';'); // разделяет строку с данными одного подписчика в массив строк

                // Присваивание элементу результирующего массива полученных данных:
                res[i].LName = data[0];
                res[i].Address = data[1];
                res[i].Journals = new Sub.Pressa[data.Length - 2];
                int indexJournals = 0;
                for (int t = 2; t < data.Length; t++)   // преобразование названий журналов в тип Pressa
                {
                    res[i][indexJournals] = (Sub.Pressa)Enum.Parse(typeof(Sub.Pressa), data[t], true);
                    indexJournals++;
                } // внутренний for
            } // внешний for

            return res;

        } // FormerArrayOfSubs

        /// <summary>
        /// Меню для выбора журнала из перечисления Sub.Pressa.
        /// </summary>
        /// <returns></returns>
        public static Sub.Pressa MenuForChoice()
        {
            Sub.Pressa current = (Sub.Pressa)0;

            bool exit = false;
            while (!exit)
            {
                Console.Clear();
                Console.WriteLine("Выберите журнал, подписчиков которого хотите увидеть (PageUp, PageDown, Enter):");
                Console.ForegroundColor = ConsoleColor.DarkRed;
                for (int i = 0; i < 5; i++)
                {
                    Sub.Pressa x = (Sub.Pressa)i;
                    Console.WriteLine("- " + x);
                }
                Console.ResetColor();
                Console.Write("Текущий журнал: ");
                Console.ForegroundColor = ConsoleColor.Red;
                Console.Write(current);
                Console.ResetColor();
                ConsoleKeyInfo but = Console.ReadKey();
                switch (but.Key)
                {
                    case ConsoleKey.PageUp:
                        if ((int)current != 0) current--;
                        break;
                    case ConsoleKey.PageDown:
                        if ((int)current != 4) current++;
                        break;
                    case ConsoleKey.Enter:
                        exit = true;
                        break;
                    default:
                        exit = true;
                        break;
                }
            } // while

            Console.Write("\n\nЖурнал выбран. Для продолжения нажмите любую клавишу . . . ");
            Console.ReadKey();
            Console.Clear();

            return current;
        } // MenuForChoice()

        /// <summary>
        /// Проверяет, содержит ли массив подписчиков subs хотя бы одного подписчика, который подписан на журнал journ.
        /// </summary>
        /// <param name="journ">Требуемый журнал.</param>
        /// <param name="subs">Массив подписчиков.</param>
        /// <returns></returns>
        public static bool IsThereSubToThisJourn(Sub.Pressa journ, Sub[] subs)
        {
            foreach (Sub s in subs)
                foreach (Sub.Pressa p in s.Journals)
                    if (p == journ)
                        return true;
            return false;
        }

        /// <summary>
        /// Выводит информацию о подписчиках на журнал journ.
        /// </summary>
        /// <param name="journ">Требуемый журнал.</param>
        /// <param name="subs">Список всех подписчиков.</param>
        public static void PrintOneJournOnTheScreen(Sub.Pressa journ, Sub[] subs)
        {
            if (!IsThereSubToThisJourn(journ, subs))
            {
                Console.WriteLine("Подписчики на журнал \"{0}\" не обнаружены!", journ);
            }
            else
            {
                Console.WriteLine("Подписчики на журнал \"{0}\":", journ);
                Console.WriteLine("┌───┬───────────────────┬─────────────────────────┬───────────────────────────┐");
                Console.WriteLine("│ № │      Фамилия      │          Адрес          │      Другие издания       │");
                Console.WriteLine("├───┼───────────────────┼─────────────────────────┼───────────────────────────┤");
                int numInTable = 0;
                for (int i = 0; i < subs.Length; i++)
                    if (subs[i].IsThereThisJourn(journ))
                        subs[i].DisplayInATable(++numInTable, journ);
                Console.WriteLine("└───┴───────────────────┴─────────────────────────┴───────────────────────────┘");
            }
            Console.Write("\nДля продолжения нажмите любую клавишу . . . ");
            Console.ReadKey();
        }

    }
}
