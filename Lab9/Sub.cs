﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;

namespace Lab_9_Task_1
{
    struct Sub : IComparable
    {
        /// <summary>
        /// Перечисление. Названия журналов.
        /// </summary>
        public enum Pressa
        {
            Крокодил, Садовод, Мурзилка, Ягодник, Лесник
        }

        string lName;
        string address;
        Pressa[] journals;

        /// <summary>
        /// Свойство для доступа к полю lName (фамилия)
        /// </summary>
        public string LName
        {
            get { return lName; }
            set { lName = value; }
        }

        /// <summary>
        /// Свойство для доступа к полю address (адрес)
        /// </summary>
        public string Address
        {
            get { return address; }
            set { address = value; }
        }

        /// <summary>
        /// Свойство для доступа к массиву перечислений journals (список выписываемых журналов)
        /// </summary>
        public Pressa[] Journals
        {
            get { return journals; }
            set { journals = value; }
        }

        /// <summary>
        /// Индексатор для доступа к массиву из перечислений journals.
        /// </summary>
        /// <param name="i"></param>
        /// <returns></returns>
        public Pressa this[int i]
        {
            get
            {
                if (i >= 0 && i < journals.Length)
                    return journals[i];
                else
                    throw new IndexOutOfRangeException();
            }
            set
            {
                if (i >= 0 && i < journals.Length)
                    journals[i] = value;
                else
                    throw new IndexOutOfRangeException();
            }
        }

        /// <summary>
        /// Выводит на экран, как строку таблицы, информацию о подписчике на журнал excludedJourn.
        /// </summary>
        /// <param name="numInTable">Номер строки в таблице.</param>
        /// <param name="excludedJourn">Журнал, о котором выводится информация.</param>
        public void DisplayInATable(int numInTable, Pressa excludedJourn)
        {
            string temp = null;
            foreach (Pressa p in this.Journals)
                if (p != excludedJourn)
                    if (temp == null)
                        temp = p.ToString();
                    else
                        temp = temp + ", " + p.ToString();

            Console.WriteLine("│{0,3}│{1,19}│{2,25}│{3,27}│", numInTable, this.LName, this.Address, (temp == null) ? "-" : temp);
        }

        /// <summary>
        /// Выводит в файл F, как строку таблицы, информацию о подписчике на журнал excludedJourn.
        /// </summary>
        /// <param name="numInTable">Номер строки в таблице.</param>
        /// <param name="excludedJourn">Журнал, о котором выводится информация.</param>
        /// <param name="F">Поток (файл), в который выводится информация.</param>
        public void DisplayInATableInAFile(int numInTable, Pressa excludedJourn, StreamWriter F)
        {
            string temp = null;
            foreach (Pressa p in this.Journals)
                if (p != excludedJourn)
                    if (temp == null)
                        temp = p.ToString();
                    else
                        temp = temp + ", " + p.ToString();

            F.WriteLine("│{0,3}│{1,19}│{2,25}│{3,27}│", numInTable, this.LName, this.Address, (temp == null) ? "-" : temp);
        }

        /// <summary>
        /// Проверяет, подписан ли подписчик на журнал journ.
        /// </summary>
        /// <param name="journ"></param>
        /// <returns></returns>
        public bool IsThereThisJourn(Pressa journ)
        {
            foreach (Pressa p in this.Journals)
                if (p == journ)
                    return true;
            return false;
        }

        // Реализация интерфейса IComparable
        public int CompareTo(object obj)
        {
            Sub temp = (Sub)obj;
            return String.Compare(this.LName, temp.LName);
        }
    }
}
