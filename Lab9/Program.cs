﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Lab_9_Task_1
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.Title = "Лабораторная работа №9 \"Структуры и перечисления\"";

            // ФОРМИРОВАНИЕ МАССИВА ПОДПИСЧИКОВ
            Sub[] subs = SubHelper.FormerArrayOfSubs(FileHelper.ReadFromFile(FileHelper.OpenFile()));

            if (subs.Length == 0)
            {
                Console.WriteLine("Не введено ни одной записи!");
                Console.ReadKey();
                return;
            }

            Console.WriteLine("\nМассив подписчиков сформирован.");
            Console.Write("\nДля продолжения нажмите любую клавишу . . . ");
            Console.ReadKey();

            // ВЫБОР ЖУРНАЛА ДЛЯ ВЫВОДА И ВЫВОД ПОДПИСЧИКОВ ЭТОГО ЖУРНАЛА
            Sub.Pressa journPrint = SubHelper.MenuForChoice();

            SubHelper.PrintOneJournOnTheScreen(journPrint, subs);
            Console.Clear();

            // ВЫВОД В ФАЙЛЫ ИНФОРМАЦИИ О КАЖДОМ ЖУРНАЛЕ
            FileHelper.PrintInAFileAllJourns(subs);

            Console.Write("\nПрограмму написал студент группы ИТ-22 Судаков Василий.");
            Console.Write("\nДля выхода нажмите любую клавишу . . . ");
            Console.ReadKey();
        }


    }
}
