﻿namespace Lab14
{
    partial class FormMain
    {
        /// <summary>
        /// Требуется переменная конструктора.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Освободить все используемые ресурсы.
        /// </summary>
        /// <param name="disposing">истинно, если управляемый ресурс должен быть удален; иначе ложно.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Код, автоматически созданный конструктором форм Windows

        /// <summary>
        /// Обязательный метод для поддержки конструктора - не изменяйте
        /// содержимое данного метода при помощи редактора кода.
        /// </summary>
        private void InitializeComponent()
        {
            this.TV_Main = new System.Windows.Forms.TreeView();
            this.folderBrowserDialog1 = new System.Windows.Forms.FolderBrowserDialog();
            this.B_CreateDirectory = new System.Windows.Forms.Button();
            this.B_DeleteDirectory = new System.Windows.Forms.Button();
            this.B_ViewTextFile = new System.Windows.Forms.Button();
            this.GB_CreateNewDirectory = new System.Windows.Forms.GroupBox();
            this.TB_NameNewDirectory = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.GB_TXT = new System.Windows.Forms.GroupBox();
            this.RB_ASCII = new System.Windows.Forms.RadioButton();
            this.RB_1251 = new System.Windows.Forms.RadioButton();
            this.RB_UTF_8 = new System.Windows.Forms.RadioButton();
            this.RB_UTF_16 = new System.Windows.Forms.RadioButton();
            this.B_Update = new System.Windows.Forms.Button();
            this.GB_CreateNewDirectory.SuspendLayout();
            this.GB_TXT.SuspendLayout();
            this.SuspendLayout();
            // 
            // TV_Main
            // 
            this.TV_Main.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.TV_Main.Location = new System.Drawing.Point(13, 13);
            this.TV_Main.Name = "TV_Main";
            this.TV_Main.Size = new System.Drawing.Size(336, 279);
            this.TV_Main.TabIndex = 0;
            // 
            // folderBrowserDialog1
            // 
            this.folderBrowserDialog1.Description = "Выберите папку для работы с ней";
            // 
            // B_CreateDirectory
            // 
            this.B_CreateDirectory.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.B_CreateDirectory.Location = new System.Drawing.Point(7, 70);
            this.B_CreateDirectory.Name = "B_CreateDirectory";
            this.B_CreateDirectory.Size = new System.Drawing.Size(127, 23);
            this.B_CreateDirectory.TabIndex = 1;
            this.B_CreateDirectory.Text = "Создать папку";
            this.B_CreateDirectory.UseVisualStyleBackColor = true;
            this.B_CreateDirectory.Click += new System.EventHandler(this.B_CreateDirectory_Click);
            // 
            // B_DeleteDirectory
            // 
            this.B_DeleteDirectory.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.B_DeleteDirectory.Location = new System.Drawing.Point(355, 118);
            this.B_DeleteDirectory.Name = "B_DeleteDirectory";
            this.B_DeleteDirectory.Size = new System.Drawing.Size(143, 36);
            this.B_DeleteDirectory.TabIndex = 2;
            this.B_DeleteDirectory.Text = "Удалить выбраную папку";
            this.B_DeleteDirectory.UseVisualStyleBackColor = true;
            this.B_DeleteDirectory.Click += new System.EventHandler(this.B_DeleteDirectory_Click);
            // 
            // B_ViewTextFile
            // 
            this.B_ViewTextFile.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.B_ViewTextFile.Location = new System.Drawing.Point(7, 73);
            this.B_ViewTextFile.Name = "B_ViewTextFile";
            this.B_ViewTextFile.Size = new System.Drawing.Size(127, 24);
            this.B_ViewTextFile.TabIndex = 3;
            this.B_ViewTextFile.Text = "Просмотреть";
            this.B_ViewTextFile.UseVisualStyleBackColor = true;
            this.B_ViewTextFile.Click += new System.EventHandler(this.B_ViewTextFile_Click);
            // 
            // GB_CreateNewDirectory
            // 
            this.GB_CreateNewDirectory.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.GB_CreateNewDirectory.Controls.Add(this.TB_NameNewDirectory);
            this.GB_CreateNewDirectory.Controls.Add(this.label1);
            this.GB_CreateNewDirectory.Controls.Add(this.B_CreateDirectory);
            this.GB_CreateNewDirectory.Location = new System.Drawing.Point(355, 12);
            this.GB_CreateNewDirectory.Name = "GB_CreateNewDirectory";
            this.GB_CreateNewDirectory.Size = new System.Drawing.Size(140, 100);
            this.GB_CreateNewDirectory.TabIndex = 5;
            this.GB_CreateNewDirectory.TabStop = false;
            this.GB_CreateNewDirectory.Text = "Создать новую папку";
            // 
            // TB_NameNewDirectory
            // 
            this.TB_NameNewDirectory.Location = new System.Drawing.Point(7, 38);
            this.TB_NameNewDirectory.Name = "TB_NameNewDirectory";
            this.TB_NameNewDirectory.Size = new System.Drawing.Size(127, 20);
            this.TB_NameNewDirectory.TabIndex = 5;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(6, 21);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(98, 13);
            this.label1.TabIndex = 4;
            this.label1.Text = "Имя новой папки:";
            // 
            // GB_TXT
            // 
            this.GB_TXT.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.GB_TXT.Controls.Add(this.RB_UTF_16);
            this.GB_TXT.Controls.Add(this.RB_UTF_8);
            this.GB_TXT.Controls.Add(this.RB_1251);
            this.GB_TXT.Controls.Add(this.RB_ASCII);
            this.GB_TXT.Controls.Add(this.B_ViewTextFile);
            this.GB_TXT.Location = new System.Drawing.Point(355, 160);
            this.GB_TXT.Name = "GB_TXT";
            this.GB_TXT.Size = new System.Drawing.Size(143, 101);
            this.GB_TXT.TabIndex = 6;
            this.GB_TXT.TabStop = false;
            this.GB_TXT.Text = "Просмотреть TXT файл";
            // 
            // RB_ASCII
            // 
            this.RB_ASCII.AutoSize = true;
            this.RB_ASCII.Checked = true;
            this.RB_ASCII.Location = new System.Drawing.Point(13, 21);
            this.RB_ASCII.Name = "RB_ASCII";
            this.RB_ASCII.Size = new System.Drawing.Size(52, 17);
            this.RB_ASCII.TabIndex = 4;
            this.RB_ASCII.TabStop = true;
            this.RB_ASCII.Text = "ASCII";
            this.RB_ASCII.UseVisualStyleBackColor = true;
            // 
            // RB_1251
            // 
            this.RB_1251.AutoSize = true;
            this.RB_1251.Location = new System.Drawing.Point(69, 21);
            this.RB_1251.Name = "RB_1251";
            this.RB_1251.Size = new System.Drawing.Size(77, 17);
            this.RB_1251.TabIndex = 5;
            this.RB_1251.Text = "1251 ANSI";
            this.RB_1251.UseVisualStyleBackColor = true;
            // 
            // RB_UTF_8
            // 
            this.RB_UTF_8.AutoSize = true;
            this.RB_UTF_8.Location = new System.Drawing.Point(13, 49);
            this.RB_UTF_8.Name = "RB_UTF_8";
            this.RB_UTF_8.Size = new System.Drawing.Size(55, 17);
            this.RB_UTF_8.TabIndex = 6;
            this.RB_UTF_8.Text = "UTF-8";
            this.RB_UTF_8.UseVisualStyleBackColor = true;
            // 
            // RB_UTF_16
            // 
            this.RB_UTF_16.AutoSize = true;
            this.RB_UTF_16.Location = new System.Drawing.Point(69, 49);
            this.RB_UTF_16.Name = "RB_UTF_16";
            this.RB_UTF_16.Size = new System.Drawing.Size(61, 17);
            this.RB_UTF_16.TabIndex = 7;
            this.RB_UTF_16.Text = "UTF-16";
            this.RB_UTF_16.UseVisualStyleBackColor = true;
            // 
            // B_Update
            // 
            this.B_Update.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.B_Update.Location = new System.Drawing.Point(355, 267);
            this.B_Update.Name = "B_Update";
            this.B_Update.Size = new System.Drawing.Size(143, 25);
            this.B_Update.TabIndex = 8;
            this.B_Update.Text = "Обновить";
            this.B_Update.UseVisualStyleBackColor = true;
            this.B_Update.Click += new System.EventHandler(this.B_Update_Click);
            // 
            // FormMain
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(507, 304);
            this.Controls.Add(this.B_Update);
            this.Controls.Add(this.GB_TXT);
            this.Controls.Add(this.GB_CreateNewDirectory);
            this.Controls.Add(this.B_DeleteDirectory);
            this.Controls.Add(this.TV_Main);
            this.Name = "FormMain";
            this.Text = "Лабораторная работа №14";
            this.GB_CreateNewDirectory.ResumeLayout(false);
            this.GB_CreateNewDirectory.PerformLayout();
            this.GB_TXT.ResumeLayout(false);
            this.GB_TXT.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TreeView TV_Main;
        private System.Windows.Forms.FolderBrowserDialog folderBrowserDialog1;
        private System.Windows.Forms.Button B_CreateDirectory;
        private System.Windows.Forms.Button B_DeleteDirectory;
        private System.Windows.Forms.Button B_ViewTextFile;
        private System.Windows.Forms.GroupBox GB_CreateNewDirectory;
        private System.Windows.Forms.TextBox TB_NameNewDirectory;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.GroupBox GB_TXT;
        private System.Windows.Forms.RadioButton RB_UTF_16;
        private System.Windows.Forms.RadioButton RB_UTF_8;
        private System.Windows.Forms.RadioButton RB_1251;
        private System.Windows.Forms.RadioButton RB_ASCII;
        private System.Windows.Forms.Button B_Update;
    }
}

