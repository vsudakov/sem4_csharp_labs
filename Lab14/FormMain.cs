﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Lab14
{
    public partial class FormMain : Form
    {
        string pathToRootDirectory;

        public FormMain()
        {
            InitializeComponent();

            while (DialogResult.OK != folderBrowserDialog1.ShowDialog());
            pathToRootDirectory = folderBrowserDialog1.SelectedPath;

            TV_Main.ImageList = new ImageList();
            TV_Main.ImageList.Images.Add(Lab14.Properties.Resources.page_white);
            TV_Main.ImageList.Images.Add(Lab14.Properties.Resources.page_white_text);
            TV_Main.ImageList.Images.Add(Lab14.Properties.Resources.folder);
            TV_Main.ImageList.Images.Add(Lab14.Properties.Resources.folder_lightbulb);

            UpdateTreeOfDirectories();

            FileSystemWatcher fsw = new FileSystemWatcher();
            fsw.Path = pathToRootDirectory;
            fsw.NotifyFilter = NotifyFilters.FileName | NotifyFilters.DirectoryName;
            fsw.Filter = "*.*";
            fsw.Renamed += new RenamedEventHandler(OnRenamed);
            fsw.EnableRaisingEvents = true;
        }

        /// <summary>
        /// Обновляет элемент TV_Main (TreeView) в соответствии с файловой системой.
        /// </summary>
        public void UpdateTreeOfDirectories()
        {
            this.TV_Main.Nodes.Clear();

            Queue<DirectoryInfo> queueDir = new Queue<DirectoryInfo>();
            Queue<TreeNode> queueTN = new Queue<TreeNode>();

            DirectoryInfo currDir = new DirectoryInfo(pathToRootDirectory);
            TreeNode currTN = new TreeNode(currDir.Name, 2, 2);
            TV_Main.Nodes.Add(currTN);
            
            while (true)
            {
                foreach (DirectoryInfo x in currDir.GetDirectories())
                {
                    TreeNode temp = new TreeNode(x.Name, 2, 2);
                    currTN.Nodes.Add(temp);

                    queueTN.Enqueue(temp);
                    queueDir.Enqueue(x);
                }
                foreach (FileInfo x in currDir.GetFiles())
                {
                    if (x.Extension == @".txt")
                        currTN.Nodes.Add(new TreeNode(x.Name, 1, 1));
                    else
                        currTN.Nodes.Add(new TreeNode(x.Name, 0, 0));
                }

                if (queueDir.Count != 0)
                {
                    currTN = queueTN.Dequeue();
                    currDir = queueDir.Dequeue();
                }
                else break;
            }
        }

        private void B_CreateDirectory_Click(object sender, EventArgs e)
        {
            TreeNode parent = TV_Main.SelectedNode;

            // сформировать путь к новой директории:
            string pathToNewDirectory = pathToRootDirectory;
            int numDels = 0;
            for(int i = pathToNewDirectory.Length - 1; i >=0; i--)
                if(pathToNewDirectory[i] != '\\') numDels++;
                else break;
            pathToNewDirectory = pathToNewDirectory.Remove(pathToNewDirectory.Length - numDels,numDels);
            pathToNewDirectory += parent.FullPath + "\\" + TB_NameNewDirectory.Text;
            // end

            Directory.CreateDirectory(pathToNewDirectory);
            MessageBox.Show("Каталог успешно создан.", "Сообщение", MessageBoxButtons.OK, MessageBoxIcon.Asterisk, MessageBoxDefaultButton.Button1);
            UpdateTreeOfDirectories();
        }

        private void B_DeleteDirectory_Click(object sender, EventArgs e)
        {
            TreeNode delDir = TV_Main.SelectedNode;

            // сформировать путь к удаляемой директории:
            string pathToDelDirectory = pathToRootDirectory;
            int numDels = 0;
            for (int i = pathToDelDirectory.Length - 1; i >= 0; i--)
                if (pathToDelDirectory[i] != '\\') numDels++;
                else break;
            pathToDelDirectory = pathToDelDirectory.Remove(pathToDelDirectory.Length - numDels, numDels);
            pathToDelDirectory += delDir.FullPath;
            // end

            Directory.Delete(pathToDelDirectory, true);
            MessageBox.Show("Каталог успешно удален\nвместе с подкаталогами и файлами.", "Сообщение", MessageBoxButtons.OK, MessageBoxIcon.Asterisk, MessageBoxDefaultButton.Button1);
            UpdateTreeOfDirectories();
        }

        private void B_ViewTextFile_Click(object sender, EventArgs e)
        {
            TreeNode curDir = TV_Main.SelectedNode;

            // сформировать путь к текстовому файлу:
            string pathToTextFile = pathToRootDirectory;
            int numDels = 0;
            for (int i = pathToTextFile.Length - 1; i >= 0; i--)
                if (pathToTextFile[i] != '\\') numDels++;
                else break;
            pathToTextFile = pathToTextFile.Remove(pathToTextFile.Length - numDels, numDels);
            pathToTextFile += curDir.FullPath;
            // end

            string res = "";
            if (RB_ASCII.Checked)
                res = File.ReadAllText(pathToTextFile, Encoding.ASCII);
            else if (RB_1251.Checked)
                res = File.ReadAllText(pathToTextFile, Encoding.Default);
            else if (RB_UTF_8.Checked)
                res = File.ReadAllText(pathToTextFile, Encoding.UTF8);
            else if (RB_UTF_16.Checked)
                res = File.ReadAllText(pathToTextFile, Encoding.Unicode);

            MessageBox.Show(res, "Содержимое текстового файла", MessageBoxButtons.OK, MessageBoxIcon.None, MessageBoxDefaultButton.Button1);
        }

        private void OnRenamed(object source, RenamedEventArgs e)
        {
            MessageBox.Show("Произошло переименование файла.\nСтарый путь: "+ e.OldFullPath +"\nНовый путь:  " + e.FullPath + "\nВНИМАНИЕ! Для обновления структуры каталогов, нажмите кнопку \"Обновить\".",
                "Сообщение", MessageBoxButtons.OK, MessageBoxIcon.Asterisk,MessageBoxDefaultButton.Button1);
        }

        private void B_Update_Click(object sender, EventArgs e)
        {
            UpdateTreeOfDirectories();
        }
    }
}
