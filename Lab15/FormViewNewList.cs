﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Lab15
{
    public partial class FormViewNewList : Form
    {
        FormMain formMain;

        public FormViewNewList(FormMain formMain)
        {
            this.formMain = formMain;

            InitializeComponent();
        }

        private void B_Show_Click(object sender, EventArgs e)
        {
            int costVerifie = (int)NUD_Cost.Value;
            DGV_Main.RowCount = 0;

            for(int i =0; i < formMain.goods.Count; i++)
                if (formMain.goods[i].CostOne <= costVerifie)
                {
                    DGV_Main.Rows.Add();
                    int lastRowIndex = DGV_Main.Rows.Count - 1;
                    DGV_Main.Rows[lastRowIndex].Cells[0].Value = formMain.goods[i].Code;
                    DGV_Main.Rows[lastRowIndex].Cells[1].Value = formMain.goods[i].Name;
                    DGV_Main.Rows[lastRowIndex].Cells[2].Value = formMain.goods[i].Supplier;
                    DGV_Main.Rows[lastRowIndex].Cells[3].Value = formMain.goods[i].CostAll;
                    DGV_Main.Rows[lastRowIndex].Cells[4].Value = formMain.goods[i].CostOne;
                    DGV_Main.Rows[lastRowIndex].Cells[5].Value = formMain.goods[i].Num;
                }
        }
    }
}
