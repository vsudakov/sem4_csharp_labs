﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Lab15
{
    public partial class FormMain : Form
    {
        public MyCollection<Product> goods;

        public FormMain()
        {
            InitializeComponent();
        }

        private void TSMI_Open_Click(object sender, EventArgs e)
        {
            openFileDialog1.InitialDirectory = Application.StartupPath.Remove(Application.StartupPath.Length - 10) + @"\DataDefault";
            if (DialogResult.OK == openFileDialog1.ShowDialog())
            {
                TSMI_Sort.Enabled = TSMI_Addictionaly.Enabled = true;

                StreamReader F = new StreamReader(openFileDialog1.FileName);

                List<Product> L = new List<Product>();

                while (!F.EndOfStream)
                {
                    string buf = F.ReadLine();
                    string[] data = buf.Split(';');

                    int code;
                    string supplier;
                    int cost;
                    int num;

                    try
                    {
                        code = Convert.ToInt32(data[0]);
                        supplier = data[1];
                        cost = Convert.ToInt32(data[2]);
                        num = Convert.ToInt32(data[3]);
                        L.Add(new Product(code, supplier, cost, num));
                    }
                    catch { }
                }

                goods = new MyCollection<Product>(L);

                Synchronize();
                TSMI_Sort.Enabled = true;
                TSMI_Addictionaly.Enabled = true;
            }
        }

        private void DGV_Main_SelectionChanged(object sender, EventArgs e)
        {
            if (DGV_Main.SelectedRows.Count != 0) TSMI_Change.Enabled = true;
            else                                  TSMI_Change.Enabled = false;
        }

        public void Synchronize()
        {
            DGV_Main.RowCount = goods.Count;

            for (int i = 0; i < goods.Count; i++)
            {
                DGV_Main.Rows[i].Cells[0].Value = goods[i].Code;
                DGV_Main.Rows[i].Cells[1].Value = goods[i].Name;
                DGV_Main.Rows[i].Cells[2].Value = goods[i].Supplier;
                DGV_Main.Rows[i].Cells[3].Value = goods[i].CostAll;
                DGV_Main.Rows[i].Cells[4].Value = goods[i].CostOne;
                DGV_Main.Rows[i].Cells[5].Value = goods[i].Num;
            }
        }

        private void TSMI_SortByCost_Click(object sender, EventArgs e)
        {
            goods.Sort(new Product.SortDescendingByCost());
            Synchronize();
        }

        private void TSMI_SortByName_Click(object sender, EventArgs e)
        {
            goods.Sort(new Product.SortDescendingByName());
            Synchronize();
        }

        private void TSMI_Change_Click(object sender, EventArgs e)
        {
            (new FormChange(this)).ShowDialog();
        }

        private void TSMI_CreateNewList_Click(object sender, EventArgs e)
        {
            (new FormViewNewList(this)).ShowDialog();
        }

        private void TSMI_Exit_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}
