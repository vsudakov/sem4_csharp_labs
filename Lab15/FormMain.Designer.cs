﻿namespace Lab15
{
    partial class FormMain
    {
        /// <summary>
        /// Требуется переменная конструктора.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Освободить все используемые ресурсы.
        /// </summary>
        /// <param name="disposing">истинно, если управляемый ресурс должен быть удален; иначе ложно.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Код, автоматически созданный конструктором форм Windows

        /// <summary>
        /// Обязательный метод для поддержки конструктора - не изменяйте
        /// содержимое данного метода при помощи редактора кода.
        /// </summary>
        private void InitializeComponent()
        {
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.TSMI_File = new System.Windows.Forms.ToolStripMenuItem();
            this.TSMI_Open = new System.Windows.Forms.ToolStripMenuItem();
            this.TSMI_Exit = new System.Windows.Forms.ToolStripMenuItem();
            this.TSMI_Change = new System.Windows.Forms.ToolStripMenuItem();
            this.TSMI_Sort = new System.Windows.Forms.ToolStripMenuItem();
            this.TSMI_SortByCost = new System.Windows.Forms.ToolStripMenuItem();
            this.TSMI_SortByName = new System.Windows.Forms.ToolStripMenuItem();
            this.TSMI_Addictionaly = new System.Windows.Forms.ToolStripMenuItem();
            this.TSMI_CreateNewList = new System.Windows.Forms.ToolStripMenuItem();
            this.openFileDialog1 = new System.Windows.Forms.OpenFileDialog();
            this.DGV_Main = new System.Windows.Forms.DataGridView();
            this.Code = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.NameProduct = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Supplier = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Cost = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.CostOne = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Num = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.menuStrip1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.DGV_Main)).BeginInit();
            this.SuspendLayout();
            // 
            // menuStrip1
            // 
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.TSMI_File,
            this.TSMI_Change,
            this.TSMI_Sort,
            this.TSMI_Addictionaly});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(608, 24);
            this.menuStrip1.TabIndex = 0;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // TSMI_File
            // 
            this.TSMI_File.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.TSMI_Open,
            this.TSMI_Exit});
            this.TSMI_File.Name = "TSMI_File";
            this.TSMI_File.Size = new System.Drawing.Size(48, 20);
            this.TSMI_File.Text = "Файл";
            // 
            // TSMI_Open
            // 
            this.TSMI_Open.Name = "TSMI_Open";
            this.TSMI_Open.Size = new System.Drawing.Size(121, 22);
            this.TSMI_Open.Text = "Открыть";
            this.TSMI_Open.Click += new System.EventHandler(this.TSMI_Open_Click);
            // 
            // TSMI_Exit
            // 
            this.TSMI_Exit.Name = "TSMI_Exit";
            this.TSMI_Exit.Size = new System.Drawing.Size(152, 22);
            this.TSMI_Exit.Text = "Выход";
            this.TSMI_Exit.Click += new System.EventHandler(this.TSMI_Exit_Click);
            // 
            // TSMI_Change
            // 
            this.TSMI_Change.Enabled = false;
            this.TSMI_Change.Name = "TSMI_Change";
            this.TSMI_Change.Size = new System.Drawing.Size(73, 20);
            this.TSMI_Change.Text = "Изменить";
            this.TSMI_Change.Click += new System.EventHandler(this.TSMI_Change_Click);
            // 
            // TSMI_Sort
            // 
            this.TSMI_Sort.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.TSMI_SortByCost,
            this.TSMI_SortByName});
            this.TSMI_Sort.Enabled = false;
            this.TSMI_Sort.Name = "TSMI_Sort";
            this.TSMI_Sort.Size = new System.Drawing.Size(163, 20);
            this.TSMI_Sort.Text = "Сортировка по убыванию";
            // 
            // TSMI_SortByCost
            // 
            this.TSMI_SortByCost.Name = "TSMI_SortByCost";
            this.TSMI_SortByCost.Size = new System.Drawing.Size(227, 22);
            this.TSMI_SortByCost.Text = "По стоимости одной штуки";
            this.TSMI_SortByCost.Click += new System.EventHandler(this.TSMI_SortByCost_Click);
            // 
            // TSMI_SortByName
            // 
            this.TSMI_SortByName.Name = "TSMI_SortByName";
            this.TSMI_SortByName.Size = new System.Drawing.Size(227, 22);
            this.TSMI_SortByName.Text = "По названию";
            this.TSMI_SortByName.Click += new System.EventHandler(this.TSMI_SortByName_Click);
            // 
            // TSMI_Addictionaly
            // 
            this.TSMI_Addictionaly.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.TSMI_CreateNewList});
            this.TSMI_Addictionaly.Enabled = false;
            this.TSMI_Addictionaly.Name = "TSMI_Addictionaly";
            this.TSMI_Addictionaly.Size = new System.Drawing.Size(107, 20);
            this.TSMI_Addictionaly.Text = "Дополнительно";
            // 
            // TSMI_CreateNewList
            // 
            this.TSMI_CreateNewList.Name = "TSMI_CreateNewList";
            this.TSMI_CreateNewList.Size = new System.Drawing.Size(462, 22);
            this.TSMI_CreateNewList.Text = "Сформировать и вывести список товаров с ценой не выше указанной";
            this.TSMI_CreateNewList.Click += new System.EventHandler(this.TSMI_CreateNewList_Click);
            // 
            // openFileDialog1
            // 
            this.openFileDialog1.FileName = "openFileDialog1";
            // 
            // DGV_Main
            // 
            this.DGV_Main.AllowUserToAddRows = false;
            this.DGV_Main.AllowUserToDeleteRows = false;
            this.DGV_Main.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.DGV_Main.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.DGV_Main.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.Code,
            this.NameProduct,
            this.Supplier,
            this.Cost,
            this.CostOne,
            this.Num});
            this.DGV_Main.Location = new System.Drawing.Point(13, 28);
            this.DGV_Main.MultiSelect = false;
            this.DGV_Main.Name = "DGV_Main";
            this.DGV_Main.ReadOnly = true;
            this.DGV_Main.RowHeadersVisible = false;
            this.DGV_Main.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.DGV_Main.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.DGV_Main.Size = new System.Drawing.Size(583, 246);
            this.DGV_Main.TabIndex = 1;
            this.DGV_Main.SelectionChanged += new System.EventHandler(this.DGV_Main_SelectionChanged);
            // 
            // Code
            // 
            this.Code.HeaderText = "Код товара";
            this.Code.Name = "Code";
            this.Code.ReadOnly = true;
            this.Code.Width = 50;
            // 
            // NameProduct
            // 
            this.NameProduct.HeaderText = "Наименование";
            this.NameProduct.Name = "NameProduct";
            this.NameProduct.ReadOnly = true;
            this.NameProduct.Width = 90;
            // 
            // Supplier
            // 
            this.Supplier.HeaderText = "Поставщик";
            this.Supplier.Name = "Supplier";
            this.Supplier.ReadOnly = true;
            this.Supplier.Width = 150;
            // 
            // Cost
            // 
            this.Cost.HeaderText = "Цена";
            this.Cost.Name = "Cost";
            this.Cost.ReadOnly = true;
            // 
            // CostOne
            // 
            this.CostOne.HeaderText = "Цена шт.";
            this.CostOne.Name = "CostOne";
            this.CostOne.ReadOnly = true;
            // 
            // Num
            // 
            this.Num.HeaderText = "Количество";
            this.Num.Name = "Num";
            this.Num.ReadOnly = true;
            // 
            // FormMain
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(608, 286);
            this.Controls.Add(this.DGV_Main);
            this.Controls.Add(this.menuStrip1);
            this.MainMenuStrip = this.menuStrip1;
            this.Name = "FormMain";
            this.Text = "Лабораторная работа №15";
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.DGV_Main)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem TSMI_File;
        private System.Windows.Forms.ToolStripMenuItem TSMI_Open;
        private System.Windows.Forms.ToolStripMenuItem TSMI_Exit;
        private System.Windows.Forms.ToolStripMenuItem TSMI_Sort;
        private System.Windows.Forms.ToolStripMenuItem TSMI_Addictionaly;
        private System.Windows.Forms.ToolStripMenuItem TSMI_CreateNewList;
        private System.Windows.Forms.OpenFileDialog openFileDialog1;
        private System.Windows.Forms.ToolStripMenuItem TSMI_Change;
        private System.Windows.Forms.ToolStripMenuItem TSMI_SortByCost;
        private System.Windows.Forms.ToolStripMenuItem TSMI_SortByName;
        private System.Windows.Forms.DataGridViewTextBoxColumn Code;
        private System.Windows.Forms.DataGridViewTextBoxColumn NameProduct;
        private System.Windows.Forms.DataGridViewTextBoxColumn Supplier;
        private System.Windows.Forms.DataGridViewTextBoxColumn Cost;
        private System.Windows.Forms.DataGridViewTextBoxColumn CostOne;
        private System.Windows.Forms.DataGridViewTextBoxColumn Num;
        public System.Windows.Forms.DataGridView DGV_Main;
    }
}

