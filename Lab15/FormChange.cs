﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Lab15
{
    public partial class FormChange : Form
    {
        FormMain formMain;

        int index;
        DataGridViewRow selectedRow;

        public FormChange(FormMain parent)
        {
            this.formMain = parent;

            InitializeComponent();

            index = formMain.DGV_Main.SelectedCells[0].RowIndex;
            selectedRow = formMain.DGV_Main.Rows[index];

            NUD_Code.Value = Convert.ToInt32(selectedRow.Cells[0].Value);
            TB_Supplier.Text = selectedRow.Cells[2].Value.ToString();
            NUD_Cost.Value = Convert.ToInt32(selectedRow.Cells[3].Value);
            NUD_Num.Value = Convert.ToInt32(selectedRow.Cells[5].Value);

        }

        private void B_Save_Click(object sender, EventArgs e)
        {
            formMain.goods[index] = new Product(Convert.ToInt32(NUD_Code.Value), TB_Supplier.Text, Convert.ToInt32(NUD_Cost.Value), Convert.ToInt32(NUD_Num.Value));
            formMain.Synchronize();
            B_Cancel.PerformClick();
        }

        private void B_Cancel_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}
