﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab15
{
    /// <summary>
    /// Перечисление наименований товара.
    /// </summary>
    enum ProductName
    {
        Диван = 101, Стул = 102, Тахта = 103, Стол = 104, Тумбочка = 105, Шкаф = 106, Комод = 107, Полка = 108
    }

    /// <summary>
    /// Структура "Товар".
    /// </summary>
    public struct Product : IComparable
    {
        /// <summary>
        /// Код товара.
        /// </summary>
        int code;

        /// <summary>
        /// Поставщик товара.
        /// </summary>
        string supplier;

        /// <summary>
        /// Стоимость всей партии товара.
        /// </summary>
        int costAll;

        /// <summary>
        /// Количество товара в партии.
        /// </summary>
        int num;

        /// <summary>
        /// Конструктор.
        /// </summary>
        /// <param name="_code"></param>
        /// <param name="_supplier"></param>
        /// <param name="_costAll"></param>
        /// <param name="_num"></param>
        public Product(int _code, string _supplier, int _costAll, int _num)
        {
            code = _code;
            supplier = _supplier;
            costAll = _costAll;
            num = _num;
        }

        /// <summary>
        /// Код товара.
        /// </summary>
        public int Code
        {
            get { return code; }
            private set { code = value; }
        }

        /// <summary>
        /// Поставщик товара.
        /// </summary>
        public string Supplier
        {
            get { return supplier; }
            private set { supplier = value; }
        }

        /// <summary>
        /// Стоимость всей партии товара.
        /// </summary>
        public int CostAll
        {
            get { return costAll; }
            private set { costAll = value; }
        }

        /// <summary>
        /// Количество товара в партии.
        /// </summary>
        public int Num
        {
            get { return num; }
            private set { num = value; }
        }

        /// <summary>
        /// Стоимость одной единицы товара.
        /// </summary>
        public double CostOne
        {
            get { return (double)CostAll / Num; }
        }

        /// <summary>
        /// Наименование товара.
        /// </summary>
        public string Name
        {
            get { return ((ProductName)Code).ToString(); }
        }

        #region IComparable Members

        public int CompareTo(object obj)
        {
            Product x = (Product)obj;

            if (Code > x.Code)
                return 1;
            if (Code < x.Code)
                return -1;
            return 0;
        }

        #endregion

        /// <summary>
        /// Класс, реализующий интерфейс IComparer, для сортировки объектов типа Product по свойству CostOne по убыванию.
        /// </summary>
        public class SortDescendingByCost : IComparer<Product>
        {
            public int Compare(Product x, Product y)
            {
                if (x.CostOne > y.CostOne)
                    return -1;
                if (x.CostOne < y.CostOne)
                    return 1;
                return 0;
            }
        }

        /// <summary>
        /// Класс, реализующий интерфейс IComparer, для сортировки объектов типа Product по свойству Nmae по убыванию.
        /// </summary>
        public class SortDescendingByName : IComparer<Product>
        {
            public int Compare(Product x, Product y)
            {
                return -String.Compare(x.Name, y.Name);
            }
        }
    }
}
