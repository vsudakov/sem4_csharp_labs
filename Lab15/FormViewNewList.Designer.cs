﻿namespace Lab15
{
    partial class FormViewNewList
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.DGV_Main = new System.Windows.Forms.DataGridView();
            this.Code = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.NameProduct = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Supplier = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Cost = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.CostOne = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Num = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.label1 = new System.Windows.Forms.Label();
            this.NUD_Cost = new System.Windows.Forms.NumericUpDown();
            this.B_Show = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.DGV_Main)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.NUD_Cost)).BeginInit();
            this.SuspendLayout();
            // 
            // DGV_Main
            // 
            this.DGV_Main.AllowUserToAddRows = false;
            this.DGV_Main.AllowUserToDeleteRows = false;
            this.DGV_Main.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.DGV_Main.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.DGV_Main.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.Code,
            this.NameProduct,
            this.Supplier,
            this.Cost,
            this.CostOne,
            this.Num});
            this.DGV_Main.Location = new System.Drawing.Point(12, 33);
            this.DGV_Main.MultiSelect = false;
            this.DGV_Main.Name = "DGV_Main";
            this.DGV_Main.ReadOnly = true;
            this.DGV_Main.RowHeadersVisible = false;
            this.DGV_Main.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.DGV_Main.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.DGV_Main.Size = new System.Drawing.Size(582, 235);
            this.DGV_Main.TabIndex = 2;
            // 
            // Code
            // 
            this.Code.HeaderText = "Код товара";
            this.Code.Name = "Code";
            this.Code.ReadOnly = true;
            this.Code.Width = 50;
            // 
            // NameProduct
            // 
            this.NameProduct.HeaderText = "Наименование";
            this.NameProduct.Name = "NameProduct";
            this.NameProduct.ReadOnly = true;
            this.NameProduct.Width = 90;
            // 
            // Supplier
            // 
            this.Supplier.HeaderText = "Поставщик";
            this.Supplier.Name = "Supplier";
            this.Supplier.ReadOnly = true;
            this.Supplier.Width = 150;
            // 
            // Cost
            // 
            this.Cost.HeaderText = "Цена";
            this.Cost.Name = "Cost";
            this.Cost.ReadOnly = true;
            // 
            // CostOne
            // 
            this.CostOne.HeaderText = "Цена шт.";
            this.CostOne.Name = "CostOne";
            this.CostOne.ReadOnly = true;
            // 
            // Num
            // 
            this.Num.HeaderText = "Количество";
            this.Num.Name = "Num";
            this.Num.ReadOnly = true;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(12, 9);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(262, 13);
            this.label1.TabIndex = 3;
            this.label1.Text = "Введите цену, в пределах которой вывести товар:";
            // 
            // NUD_Cost
            // 
            this.NUD_Cost.Location = new System.Drawing.Point(280, 7);
            this.NUD_Cost.Maximum = new decimal(new int[] {
            1316134911,
            2328,
            0,
            0});
            this.NUD_Cost.Name = "NUD_Cost";
            this.NUD_Cost.Size = new System.Drawing.Size(120, 20);
            this.NUD_Cost.TabIndex = 4;
            // 
            // B_Show
            // 
            this.B_Show.Location = new System.Drawing.Point(406, 6);
            this.B_Show.Name = "B_Show";
            this.B_Show.Size = new System.Drawing.Size(75, 23);
            this.B_Show.TabIndex = 5;
            this.B_Show.Text = "Показать";
            this.B_Show.UseVisualStyleBackColor = true;
            this.B_Show.Click += new System.EventHandler(this.B_Show_Click);
            // 
            // FormViewNewList
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(606, 280);
            this.Controls.Add(this.B_Show);
            this.Controls.Add(this.NUD_Cost);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.DGV_Main);
            this.Name = "FormViewNewList";
            ((System.ComponentModel.ISupportInitialize)(this.DGV_Main)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.NUD_Cost)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        public System.Windows.Forms.DataGridView DGV_Main;
        private System.Windows.Forms.DataGridViewTextBoxColumn Code;
        private System.Windows.Forms.DataGridViewTextBoxColumn NameProduct;
        private System.Windows.Forms.DataGridViewTextBoxColumn Supplier;
        private System.Windows.Forms.DataGridViewTextBoxColumn Cost;
        private System.Windows.Forms.DataGridViewTextBoxColumn CostOne;
        private System.Windows.Forms.DataGridViewTextBoxColumn Num;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.NumericUpDown NUD_Cost;
        private System.Windows.Forms.Button B_Show;
    }
}