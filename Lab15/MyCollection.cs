﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab15
{
    /// <summary>
    /// Коллекция с неизменяемым количеством данных.
    /// </summary>
    /// <typeparam name="T">Любой значимый тип, реализующий интерфейс IComparable.</typeparam>
    public class MyCollection<T>
        where T:struct,IComparable
    {
        /// <summary>
        /// Поле-массив для хранения данных.
        /// </summary>
        T[] data;

        /// <summary>
        /// Конструктор.
        /// </summary>
        /// <param name="data">Источник данных для коллекции.</param>
        public MyCollection(ICollection<T> data)
        {
            this.data = new T[data.Count];

            int index = 0;
            foreach (T x in data)
                this[index++] = x;
        }

        /// <summary>
        /// Индексатор.
        /// </summary>
        /// <param name="i"></param>
        /// <returns></returns>
        public T this[int i]
        {
            get { return data[i];}
            set { data[i] = value; }
        }

        /// <summary>
        /// Количество товара.
        /// </summary>
        public int Count
        {
            get { return data.Length; }
        }

        /// <summary>
        /// Сортирует элементы коллекции с помощью компаратора.
        /// </summary>
        /// <param name="iC"></param>
        public void Sort(IComparer<T> iC)
        {
            Array.Sort<T>(this.data, iC);
        }
    }
}
