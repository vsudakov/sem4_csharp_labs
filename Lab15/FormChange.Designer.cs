﻿namespace Lab15
{
    partial class FormChange
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.TB_Supplier = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.NUD_Code = new System.Windows.Forms.NumericUpDown();
            this.NUD_Cost = new System.Windows.Forms.NumericUpDown();
            this.NUD_Num = new System.Windows.Forms.NumericUpDown();
            this.B_Save = new System.Windows.Forms.Button();
            this.B_Cancel = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.NUD_Code)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.NUD_Cost)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.NUD_Num)).BeginInit();
            this.SuspendLayout();
            // 
            // TB_Supplier
            // 
            this.TB_Supplier.Location = new System.Drawing.Point(86, 40);
            this.TB_Supplier.Name = "TB_Supplier";
            this.TB_Supplier.Size = new System.Drawing.Size(119, 20);
            this.TB_Supplier.TabIndex = 1;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(13, 9);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(67, 13);
            this.label1.TabIndex = 4;
            this.label1.Text = "Код товара:";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(12, 43);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(68, 13);
            this.label2.TabIndex = 5;
            this.label2.Text = "Поставщик:";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(44, 75);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(36, 13);
            this.label3.TabIndex = 6;
            this.label3.Text = "Цена:";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(11, 109);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(69, 13);
            this.label4.TabIndex = 7;
            this.label4.Text = "Количество:";
            // 
            // NUD_Code
            // 
            this.NUD_Code.Location = new System.Drawing.Point(85, 7);
            this.NUD_Code.Maximum = new decimal(new int[] {
            108,
            0,
            0,
            0});
            this.NUD_Code.Minimum = new decimal(new int[] {
            101,
            0,
            0,
            0});
            this.NUD_Code.Name = "NUD_Code";
            this.NUD_Code.Size = new System.Drawing.Size(120, 20);
            this.NUD_Code.TabIndex = 8;
            this.NUD_Code.Value = new decimal(new int[] {
            101,
            0,
            0,
            0});
            // 
            // NUD_Cost
            // 
            this.NUD_Cost.Location = new System.Drawing.Point(86, 73);
            this.NUD_Cost.Maximum = new decimal(new int[] {
            1215752191,
            23,
            0,
            0});
            this.NUD_Cost.Name = "NUD_Cost";
            this.NUD_Cost.Size = new System.Drawing.Size(120, 20);
            this.NUD_Cost.TabIndex = 9;
            // 
            // NUD_Num
            // 
            this.NUD_Num.Location = new System.Drawing.Point(85, 107);
            this.NUD_Num.Maximum = new decimal(new int[] {
            1215752191,
            23,
            0,
            0});
            this.NUD_Num.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.NUD_Num.Name = "NUD_Num";
            this.NUD_Num.Size = new System.Drawing.Size(120, 20);
            this.NUD_Num.TabIndex = 10;
            this.NUD_Num.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            // 
            // B_Save
            // 
            this.B_Save.Location = new System.Drawing.Point(25, 144);
            this.B_Save.Name = "B_Save";
            this.B_Save.Size = new System.Drawing.Size(75, 23);
            this.B_Save.TabIndex = 11;
            this.B_Save.Text = "Сохранить";
            this.B_Save.UseVisualStyleBackColor = true;
            this.B_Save.Click += new System.EventHandler(this.B_Save_Click);
            // 
            // B_Cancel
            // 
            this.B_Cancel.Location = new System.Drawing.Point(120, 144);
            this.B_Cancel.Name = "B_Cancel";
            this.B_Cancel.Size = new System.Drawing.Size(75, 23);
            this.B_Cancel.TabIndex = 12;
            this.B_Cancel.Text = "Отмена";
            this.B_Cancel.UseVisualStyleBackColor = true;
            this.B_Cancel.Click += new System.EventHandler(this.B_Cancel_Click);
            // 
            // FormChange
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(219, 175);
            this.ControlBox = false;
            this.Controls.Add(this.B_Cancel);
            this.Controls.Add(this.B_Save);
            this.Controls.Add(this.NUD_Num);
            this.Controls.Add(this.NUD_Cost);
            this.Controls.Add(this.NUD_Code);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.TB_Supplier);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.Name = "FormChange";
            this.Text = "Изменить";
            ((System.ComponentModel.ISupportInitialize)(this.NUD_Code)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.NUD_Cost)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.NUD_Num)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox TB_Supplier;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.NumericUpDown NUD_Code;
        private System.Windows.Forms.NumericUpDown NUD_Cost;
        private System.Windows.Forms.NumericUpDown NUD_Num;
        private System.Windows.Forms.Button B_Save;
        private System.Windows.Forms.Button B_Cancel;
    }
}