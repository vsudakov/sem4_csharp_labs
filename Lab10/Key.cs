﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Lab10
{
    /// <summary>
    /// Класс-источник
    /// </summary>
    static class Key
    {
        /// <summary>
        /// Массив делегатов
        /// </summary>
        static Del[] dels;

        /// <summary>
        /// Массив делегатов
        /// </summary>
        public static Del[] Dels
        {
            set { dels = value; }
        }

        /// <summary>
        /// Событие
        /// </summary>
        public static event Del mainEvent
        {
            add // способ добавления обработчика
            {
                for (int i = 0; i < dels.Length; i++)
                    if (dels[i] == null)
                    {
                        dels[i] = value;
                        break;
                    }
            }
            remove // способ удаления обработчика
            {
                for (int i = 0; i < dels.Length; i++)
                    if (dels[i] == value)
                    {
                        dels[i] = null;
                        break;
                    }
            }
        }

        /// <summary>
        /// Метод инициирующий событие.
        /// </summary>
        /// <param name="nameOfMethod">Название метода.</param>
        public static void OnPressKey(string nameOfMethod)
        {
            foreach (Del x in dels)
                if (x != null)
                    if (nameOfMethod == x.Method.Name)
                        x();
        }
    }
}
