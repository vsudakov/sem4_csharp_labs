﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Lab10
{
    class Student : IComparable
    {
        string lastName;
        string nameFac;
        int stipend;
        int[] marks;

        /// <summary>
        /// Конструктор с параметрами.
        /// </summary>
        /// <param name="lastName">Фамилия.</param>
        /// <param name="nameFac">Название факультета.</param>
        /// <param name="stipend">Стипендия.</param>
        /// <param name="marks">Оценки.</param>
        public Student(string lastName, string nameFac, int stipend, params int[] marks)
        {
            this.LastName = lastName;
            this.NameFac = nameFac;
            this.Stipend = stipend;
            this.Marks = marks;
        }

        /// <summary>
        /// Свойство для доступа к полю lastName
        /// </summary>
        public string LastName
        {
            get { return lastName; }
            set { lastName = value; }
        }

        /// <summary>
        /// Свойство для доступа к полю nameFac
        /// </summary>
        public string NameFac
        {
            get { return nameFac; }
            set { nameFac = value; }
        }

        /// <summary>
        /// Свойство для доступа к полю stipend
        /// </summary>
        public int Stipend
        {
            get { return stipend; }
            set
            {
                if (value >= 0) stipend = value;
                else stipend = -1;
            }
        }

        /// <summary>
        /// Свойство для доступа к полю-массиву marks
        /// </summary>
        public int[] Marks
        {
            get { return marks; }
            set { marks = value; }
        }

        /// <summary>
        /// Определяет является ли студент, представляемый данным объектом, двоечником.
        /// </summary>
        public bool IsBadStudent
        {
            get
            {
                foreach (var x in Marks)
                    if (x < 4)
                        return true;
                return false;
            }
        }

        /// <summary>
        /// Метод-реакция на событие источника.
        /// </summary>
        public void OnPageDown()
        {
            Stipend = Stipend * 8 / 10;
        }

        /// <summary>
        /// Метод-реакция на событие источника
        /// </summary>
        public void OnPageUp()
        {
            Stipend = Stipend / 10 * 12;
        }

        /// <summary>
        /// Выводит информацию о экзкмпляре в строку таблицы.
        /// </summary>
        /// <param name="numInTable">Номер строки в таблице.</param>
        public void Output(int numInTable)
        {
            string buf = null;
            foreach (int x in Marks)
                buf = buf + ' ' + x;

            Console.WriteLine("│{0,3}│{1,18}│{2,16}│{3,9}│{4,11}│", numInTable, LastName, buf, NameFac, Stipend);
        }

        /// <summary>
        /// Метод, благодаря которому реализуется интерфейс IComparable.
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        public int CompareTo(object obj)
        {
            return this.NameFac.CompareTo(((Student)obj).NameFac);
        }
    }
}
