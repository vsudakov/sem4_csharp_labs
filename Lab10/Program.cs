﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Lab10
{
    /// <summary>
    /// Объявление делегата
    /// </summary>
    public delegate void Del();

    class Program
    {
        static void Main(string[] args)
        {
            Student[] studs = StudentHelper.FormerArrayOfStudents(FileHelper.ReadFromFile(FileHelper.OpenFile())); // Формирование массива студентов
            Array.Sort(studs); // сортирует массив объектов по полю nameFac, используя интерфейс IComparable
            StudentHelper.RegOfStudents(studs); // "регистрация студентов"

            Console.WriteLine("\nСчитанные данные из файла:");
            StudentHelper.OutputAll(studs);
            Console.WriteLine();

            bool exit = false;
            while (!exit)
            {
                Console.WriteLine("\nВведите одну из клавиш (PageUp, PageDown, Delete, Escape): ");
                ConsoleKeyInfo but = Console.ReadKey();
                switch (but.Key)
                {
                    case ConsoleKey.PageUp:
                        Key.OnPressKey("OnPageUp");
                        Console.WriteLine("\nСтипендия повышена на 20%:");
                        StudentHelper.OutputAll(studs);
                        break;
                    case ConsoleKey.PageDown:
                        Key.OnPressKey("OnPageDown");
                        Console.WriteLine("\nСтипендия понижена на 20%:");
                        StudentHelper.OutputAll(studs);
                        break;
                    case ConsoleKey.Delete:
                        studs = StudentHelper.DeleteBadStudents(studs);
                        Console.WriteLine("\nУдалены все двоечники:");
                        StudentHelper.OutputAll(studs);
                        break;
                    case ConsoleKey.Escape:
                        exit = true;
                        break;
                }
            }

            Console.Write("\nПрограмму написал студент группы ИТ-22 Судаков Василий.");
            Console.Write("\nДля выхода нажмите любую клавишу . . . ");
            Console.ReadKey();
        }
    }
}
