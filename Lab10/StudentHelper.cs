﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Lab10
{
    class StudentHelper
    {
        /// <summary>
        /// Возвращает массив объектов класса Student, сформированный из данных, содержащиеся в списке data.
        /// </summary>
        /// <param name="data">Исходные данные.</param>
        /// <returns></returns>
        public static Student[] FormerArrayOfStudents(List<string> data)
        {
            Student[] res = new Student[data.Count];

            for (int i = 0; i < data.Count; i++)
            {
                string[] buf = data[i].Split(';');

                int[] bufMarks = new int[buf.Length - 3];       // формирование массива оценок
                for (int j = 0, t = 3; t < buf.Length; t++, j++)
                    bufMarks[j] = Convert.ToInt32(buf[t]);

                res[i] = new Student(buf[0], buf[1], Convert.ToInt32(buf[2]), bufMarks);
            }

            return res;
        }

        /// <summary>
        /// Добавление обработчиков события.("Регистрация объектов Student")
        /// </summary>
        /// <param name="studs">Массив объектов Student</param>
        public static void RegOfStudents(Student[] studs)
        {
            Key.Dels = new Del[studs.Length * 2];

            foreach (Student x in studs)
            {
                Key.mainEvent += new Del(x.OnPageDown);
                Key.mainEvent += new Del(x.OnPageUp);
            }
        }

        /// <summary>
        /// Возвращает массив объектов класса Student без "двоечников".
        /// </summary>
        /// <param name="studs">Массив объектов.</param>
        public static Student[] DeleteBadStudents(Student[] studs)
        {
            List<Student> L = new List<Student>();

            foreach (var s in studs)
                if (s.IsBadStudent)
                {
                    Key.mainEvent -= s.OnPageUp;
                    Key.mainEvent -= s.OnPageDown;
                }
                else
                    L.Add(s);

            Student[] res = new Student[L.Count];
            for (int i = 0; i < L.Count; i++)
                res[i] = L[i];

            return res;
        }

        /// <summary>
        /// Выводит информацию о каждом объекте класса Student массива studs в виде таблицы.
        /// </summary>
        /// <param name="studs">Массив объектов класса Student.</param>
        public static void OutputAll(Student[] studs)
        {
            if (studs == null)
            {
                Console.WriteLine("Студенты отсутствуют!");
            }
            else
            {
                Console.WriteLine("┌───┬──────────────────┬────────────────┬─────────┬───────────┐");
                Console.WriteLine("│ № │     Фамилия      │Оценки за сессию│ Деканат │ Стипендия │");
                Console.WriteLine("├───┼──────────────────┼────────────────┼─────────┼───────────┤");
                for (int i = 0; i < studs.Length; i++)
                    studs[i].Output(i + 1);
                Console.WriteLine("└───┴──────────────────┴────────────────┴─────────┴───────────┘");
            }
        }
    }
}
