﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Lab13
{
    public partial class FormMain : Form
    {
        GameLogic gameLogic;
        GameDraw gameDraw;

        bool isCrossOrZero;
        bool isGameInProcess;

        public FormMain()
        {
            InitializeComponent();

            isGameInProcess = false;
        }

        private void B_NewGame_Click(object sender, EventArgs e)
        {
            B_NewGame.Visible = false;
            P_Gaming.Visible = true;

            isCrossOrZero = true;
            isGameInProcess = true;

            gameLogic = new GameLogic(P_Game);
            gameDraw = new GameDraw(P_Game, P_Icon);

            gameDraw.UpdateIcon(isCrossOrZero);
        }

        private void P_Game_MouseClick(object sender, MouseEventArgs e)
        {
            if (isGameInProcess)
            {
                try
                {
                    if (gameLogic.Click(e.X, e.Y, isCrossOrZero))
                    {
                        gameDraw.Click(e.X, e.Y, isCrossOrZero);
                        isCrossOrZero = !isCrossOrZero;
                        gameDraw.UpdateIcon(isCrossOrZero);
                    }
                }
                catch (Victory)
                {
                    gameDraw.Click(e.X, e.Y, isCrossOrZero);

                    isGameInProcess = false;
                    B_NewGame.Visible = true;
                    P_Gaming.Visible = false;
                    
                    MessageBox.Show("Победили " + ((isCrossOrZero) ? "крестики" : "нолики") + "!", "Победа!",
                        MessageBoxButtons.OK, MessageBoxIcon.Asterisk, MessageBoxDefaultButton.Button1);
                }
            }
        }
    }
}
