﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Drawing;
using System.Windows.Forms;

namespace Lab13
{
    class GameDraw
    {
        /// <summary>
        /// Поверхность главной доски игры.
        /// </summary>
        Graphics graphGame;

        /// <summary>
        /// Иконка крестика.
        /// </summary>
        Graphics graphIcon;

        /// <summary>
        /// Длина стороны доски.
        /// </summary>
        float sizeSideBoard;

        /// <summary>
        /// Размер стороны одной клеточки.
        /// </summary>
        float sizeSideSquare;

        /// <summary>
        /// Размер стороны иконки крестка либо нолика.
        /// </summary>
        float sizeSideIcon;

        public GameDraw(Panel panel, Panel panelIcon)
        {
            graphGame = panel.CreateGraphics();
            graphIcon = panelIcon.CreateGraphics();

            sizeSideBoard = panel.Height;
            sizeSideSquare = (float)panel.Height / 13;
            sizeSideIcon = panelIcon.Height;

            graphGame.Clear(Color.Wheat);
            DrawGrid();
        }

        ///// <summary>
        ///// Очищает игровое поле.
        ///// </summary>
        //public void NewGame()
        //{
        //    graphGame.Clear(Color.Wheat);

        //    DrawGrid();
        //}

        /// <summary>
        /// Рисует сетку.
        /// </summary>
        public void DrawGrid()
        {
            Pen pen = new Pen(Color.Brown,2);

            for (int i = 0; i <= 13; i++)
            {
                graphGame.DrawLine(pen, i * sizeSideSquare, 0, i * sizeSideSquare, sizeSideBoard);
                graphGame.DrawLine(pen, 0, i * sizeSideSquare, sizeSideBoard, i * sizeSideSquare);
            }
        }

        /// <summary>
        /// Рисует крестик либо нолик на игровой доске при нажатии мыши.
        /// </summary>
        /// <param name="X">Координата X мыши.</param>
        /// <param name="Y">Координата Y мыши.</param>
        /// <param name="isCrossOrZero"></param>
        public void Click(float X, float Y, bool isCrossOrZero)
        {
            int coordJ = (int)Math.Truncate((double)(X / sizeSideSquare));
            int coordI = (int)Math.Truncate((double)(Y / sizeSideSquare));

            if (isCrossOrZero)
            {
                Pen pen = new Pen(Color.Black,3);

                graphGame.DrawLine(pen, coordJ * sizeSideSquare + 3, coordI * sizeSideSquare + 3, (coordJ + 1) * sizeSideSquare - 3, (coordI + 1) * sizeSideSquare -3);
                graphGame.DrawLine(pen, coordJ * sizeSideSquare +3, (coordI + 1) * sizeSideSquare - 3, (coordJ + 1) * sizeSideSquare -3, coordI * sizeSideSquare +3);
            }
            else
            {
                Pen pen = new Pen(Color.DarkOrchid, 3);

                graphGame.DrawEllipse(pen, coordJ * sizeSideSquare + 3, coordI * sizeSideSquare + 3, sizeSideSquare - 6, sizeSideSquare - 6);
            }
        }

        /// <summary>
        /// Рисует иконки игрока, который должен ходить.
        /// </summary>
        /// <param name="isCrossOrZero"></param>
        public void UpdateIcon(bool isCrossOrZero)
        {
            Pen pen = new Pen(Color.Black, 8);
            if (!isCrossOrZero) pen.Color = Color.DarkOrchid;

            graphIcon.Clear(Color.SandyBrown);

            if (isCrossOrZero)
            {
                graphIcon.DrawLine(pen, 10, 10, sizeSideIcon - 10, sizeSideIcon - 10);
                graphIcon.DrawLine(pen, 10, sizeSideIcon - 10, sizeSideIcon - 10, 10);
            }
            else
            {
                graphIcon.DrawEllipse(pen, 10, 10, sizeSideIcon - 20, sizeSideIcon - 20);
            }
        }
    }
}
