﻿namespace Lab13
{
    partial class FormMain
    {
        /// <summary>
        /// Требуется переменная конструктора.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Освободить все используемые ресурсы.
        /// </summary>
        /// <param name="disposing">истинно, если управляемый ресурс должен быть удален; иначе ложно.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Код, автоматически созданный конструктором форм Windows

        /// <summary>
        /// Обязательный метод для поддержки конструктора - не изменяйте
        /// содержимое данного метода при помощи редактора кода.
        /// </summary>
        private void InitializeComponent()
        {
            this.P_Game = new System.Windows.Forms.Panel();
            this.label1 = new System.Windows.Forms.Label();
            this.P_Icon = new System.Windows.Forms.Panel();
            this.P_Gaming = new System.Windows.Forms.Panel();
            this.B_NewGame = new System.Windows.Forms.Button();
            this.B_NewGameLittle = new System.Windows.Forms.Button();
            this.P_Gaming.SuspendLayout();
            this.SuspendLayout();
            // 
            // P_Game
            // 
            this.P_Game.BackColor = System.Drawing.Color.Wheat;
            this.P_Game.Location = new System.Drawing.Point(39, 12);
            this.P_Game.Name = "P_Game";
            this.P_Game.Size = new System.Drawing.Size(332, 332);
            this.P_Game.TabIndex = 0;
            this.P_Game.MouseClick += new System.Windows.Forms.MouseEventHandler(this.P_Game_MouseClick);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Comic Sans MS", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label1.Location = new System.Drawing.Point(135, 16);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(83, 58);
            this.label1.TabIndex = 0;
            this.label1.Text = "Сейчас\r\n ходят:";
            // 
            // P_Icon
            // 
            this.P_Icon.Location = new System.Drawing.Point(242, 0);
            this.P_Icon.Name = "P_Icon";
            this.P_Icon.Size = new System.Drawing.Size(90, 90);
            this.P_Icon.TabIndex = 2;
            // 
            // P_Gaming
            // 
            this.P_Gaming.Controls.Add(this.B_NewGameLittle);
            this.P_Gaming.Controls.Add(this.P_Icon);
            this.P_Gaming.Controls.Add(this.label1);
            this.P_Gaming.Location = new System.Drawing.Point(39, 351);
            this.P_Gaming.Name = "P_Gaming";
            this.P_Gaming.Size = new System.Drawing.Size(332, 90);
            this.P_Gaming.TabIndex = 4;
            this.P_Gaming.Visible = false;
            // 
            // B_NewGame
            // 
            this.B_NewGame.BackColor = System.Drawing.Color.IndianRed;
            this.B_NewGame.Font = new System.Drawing.Font("Comic Sans MS", 21.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.B_NewGame.Location = new System.Drawing.Point(39, 352);
            this.B_NewGame.Name = "B_NewGame";
            this.B_NewGame.Size = new System.Drawing.Size(332, 90);
            this.B_NewGame.TabIndex = 0;
            this.B_NewGame.Text = "НОВАЯ ИГРА";
            this.B_NewGame.UseVisualStyleBackColor = false;
            this.B_NewGame.Click += new System.EventHandler(this.B_NewGame_Click);
            // 
            // B_NewGameLittle
            // 
            this.B_NewGameLittle.BackColor = System.Drawing.Color.IndianRed;
            this.B_NewGameLittle.Font = new System.Drawing.Font("Comic Sans MS", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.B_NewGameLittle.Location = new System.Drawing.Point(0, 0);
            this.B_NewGameLittle.Name = "B_NewGameLittle";
            this.B_NewGameLittle.Size = new System.Drawing.Size(107, 91);
            this.B_NewGameLittle.TabIndex = 1;
            this.B_NewGameLittle.Text = "НОВАЯ ИГРА";
            this.B_NewGameLittle.UseVisualStyleBackColor = false;
            this.B_NewGameLittle.Click += new System.EventHandler(this.B_NewGame_Click);
            // 
            // FormMain
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.SandyBrown;
            this.ClientSize = new System.Drawing.Size(412, 450);
            this.Controls.Add(this.B_NewGame);
            this.Controls.Add(this.P_Gaming);
            this.Controls.Add(this.P_Game);
            this.Name = "FormMain";
            this.Text = "Пять в ряд";
            this.P_Gaming.ResumeLayout(false);
            this.P_Gaming.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel P_Game;
        private System.Windows.Forms.Button B_NewGame;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Panel P_Icon;
        private System.Windows.Forms.Panel P_Gaming;
        private System.Windows.Forms.Button B_NewGameLittle;
    }
}

