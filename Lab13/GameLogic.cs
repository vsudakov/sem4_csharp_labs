﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Lab13
{
    class GameLogic
    {
        /// <summary>
        /// Матрица поставленых значков.
        /// Значение ячейки : true - крестик
        ///                   false - нолик
        /// </summary>
        bool?[,] board;

        /// <summary>
        /// Длина стороны доски.
        /// </summary>
        float sizeSideBoard;

        /// <summary>
        /// Размер стороны одной клеточки.
        /// </summary>
        float sizeSideSquare;

        /// <summary>
        /// Конструктор.
        /// </summary>
        /// <param name="panel">Главная панель игры.</param>
        public GameLogic(Panel panel)
        {
            board = new bool?[13, 13];
            sizeSideBoard = panel.Height;
            sizeSideSquare = sizeSideBoard / 13;
        }

        /// <summary>
        /// Добавляет ход.
        /// </summary>
        /// <param name="X">Координата X клика.</param>
        /// <param name="Y">Координата Y клика.</param>
        /// <param name="isCrossOrZero">True - поставлен крестик; False - поставлен нолик.</param>
        /// <returns>Возвращает: true - ход зачислен
        ///                      false - ход не зачислен</returns>
        public bool Click(float X, float Y, bool isCrossOrZero)
        {
            int coordJ = (int)Math.Truncate((double)(X / sizeSideSquare));
            int coordI = (int)Math.Truncate((double)(Y / sizeSideSquare));

            if (board[coordI, coordJ].HasValue)
            {
                return false; // в этой ячейке уже стоит значок.
            }
            else
            {
                board[coordI, coordJ] = isCrossOrZero;

                if (CheckUpAndDown(coordI, coordJ, isCrossOrZero) ||
                    CheckLeftAndRight(coordI, coordJ, isCrossOrZero) ||
                    CheckLeftUpAndRightDown(coordI, coordJ, isCrossOrZero) ||
                    CheckLeftDownAndRightUp(coordI, coordJ, isCrossOrZero)
                    )
                    throw new Victory();

                return true;
            }
        }

        /// <summary>
        /// Проверяет направления вверх и вниз.
        /// </summary>
        /// <param name="I"></param>
        /// <param name="J"></param>
        /// <param name="isCrossOrZero"></param>
        /// <returns></returns>
        private bool CheckUpAndDown(int I, int J,bool isCrossOrZero)
        {
            int counter = 0; // счётчик

            for (int i = I; i >= 0; i--)
                if (board[i, J].HasValue)
                    if (board[i, J].Value == isCrossOrZero) counter++;
                    else break;
                else break;

            for (int i = I + 1; i < 13 ; i++)
                if (board[i, J].HasValue)
                    if (board[i, J].Value == isCrossOrZero) counter++;
                    else break;
                else break;
            
            if (counter >= 5) return true;
            else return false;
        }

        /// <summary>
        /// Проверяет направления влево и вправо.
        /// </summary>
        /// <param name="I"></param>
        /// <param name="J"></param>
        /// <param name="isCrossOrZero"></param>
        /// <returns></returns>
        private bool CheckLeftAndRight(int I, int J, bool isCrossOrZero)
        {
            int counter = 0; // счётчик

            for (int j = J; j >= 0; j--)
                if (board[I, j].HasValue)
                    if (board[I, j].Value == isCrossOrZero) counter++;
                    else break;
                else break;

            for (int j = J + 1; j < 13; j++)
                if (board[I, j].HasValue)
                    if (board[I, j].Value == isCrossOrZero) counter++;
                    else break;
                else break;

            if (counter >= 5) return true;
            else return false;
        }

        /// <summary>
        /// Проверяет направления по диагонали влево-вверх и вправо-вниз.
        /// </summary>
        /// <param name="I"></param>
        /// <param name="J"></param>
        /// <param name="isCrossOrZero"></param>
        /// <returns></returns>
        private bool CheckLeftUpAndRightDown(int I, int J, bool isCrossOrZero)
        {
            int counter = 0; // счётчик
            int i;
            int j;

            for (i = I, j = J; (i >= 0) && (j >= 0); i--, j--)
                if (board[i, j].HasValue)
                    if (board[i, j].Value == isCrossOrZero) counter++;
                    else break;
                else break;

            for(i = I+1, j = J+1; (i < 13) && (j < 13); i++, j++)
                if(board[i,j].HasValue)
                    if(board[i,j].Value == isCrossOrZero) counter++;
                    else break;
                else break;

            if(counter >= 5) return true;
            else return false;
        }

        /// <summary>
        /// Проверяет направления по диагонали влево-вниз и вправо-вверх.
        /// </summary>
        /// <param name="I"></param>
        /// <param name="J"></param>
        /// <param name="isCrossOrZero"></param>
        /// <returns></returns>
        private bool CheckLeftDownAndRightUp(int I, int J, bool isCrossOrZero)
        {
            int counter = 0; // счётчик
            int i;
            int j;

            for (i = I, j = J; (i < 13) && (j >= 0); i++, j--)
                if (board[i, j].HasValue)
                    if (board[i, j].Value == isCrossOrZero) counter++;
                    else break;
                else break;

            for (i = I - 1, j = J + 1; (i >= 0) && (j <13); i--, j++)
                if (board[i, j].HasValue)
                    if (board[i, j].Value == isCrossOrZero) counter++;
                    else break;
                else break;

            if (counter >= 5) return true;
            else return false;
        }
    }
}
